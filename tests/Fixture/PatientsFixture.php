<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PatientsFixture
 */
class PatientsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'staff_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'gender_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'marital_status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'huntington_status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'accommodation_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'employment_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'huntington_inheritance_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'living_arrangement_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'death_cause_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'first_name' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'middle_name' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'last_name' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dob' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'country_of_birth' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'if_research_participant' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'deceased_year' => ['type' => 'string', 'length' => 4, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'annual_income' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'preferred_language' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'is_indigenous_or_islander' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'date_captured' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'deleted' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'post_code' => ['type' => 'string', 'length' => 4, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'accommodation_type_id' => ['type' => 'index', 'columns' => ['accommodation_type_id'], 'length' => []],
            'death_cause_id' => ['type' => 'index', 'columns' => ['death_cause_id'], 'length' => []],
            'employment_type_id' => ['type' => 'index', 'columns' => ['employment_type_id'], 'length' => []],
            'gender_id' => ['type' => 'index', 'columns' => ['gender_id'], 'length' => []],
            'huntington_inheritance_id' => ['type' => 'index', 'columns' => ['huntington_inheritance_id'], 'length' => []],
            'huntington_status_id' => ['type' => 'index', 'columns' => ['huntington_status_id'], 'length' => []],
            'living_arrangement_id' => ['type' => 'index', 'columns' => ['living_arrangement_id'], 'length' => []],
            'marital_status_id' => ['type' => 'index', 'columns' => ['marital_status_id'], 'length' => []],
            'staff_id' => ['type' => 'index', 'columns' => ['staff_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'patients_ibfk_1' => ['type' => 'foreign', 'columns' => ['staff_id'], 'references' => ['staff', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_2' => ['type' => 'foreign', 'columns' => ['gender_id'], 'references' => ['genders', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_3' => ['type' => 'foreign', 'columns' => ['marital_status_id'], 'references' => ['marital_statuses', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_4' => ['type' => 'foreign', 'columns' => ['huntington_status_id'], 'references' => ['huntington_statuses', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_5' => ['type' => 'foreign', 'columns' => ['accommodation_type_id'], 'references' => ['accommodation_types', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_6' => ['type' => 'foreign', 'columns' => ['employment_type_id'], 'references' => ['employment_types', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_7' => ['type' => 'foreign', 'columns' => ['huntington_inheritance_id'], 'references' => ['huntington_inheritances', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_8' => ['type' => 'foreign', 'columns' => ['living_arrangement_id'], 'references' => ['living_arrangements', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'patients_ibfk_9' => ['type' => 'foreign', 'columns' => ['death_cause_id'], 'references' => ['death_causes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'staff_id' => 1,
                'gender_id' => 1,
                'marital_status_id' => 1,
                'huntington_status_id' => 1,
                'accommodation_type_id' => 1,
                'employment_type_id' => 1,
                'huntington_inheritance_id' => 1,
                'living_arrangement_id' => 1,
                'death_cause_id' => 1,
                'first_name' => 'Lorem ipsum dolor sit amet',
                'middle_name' => 'Lorem ipsum dolor sit amet',
                'last_name' => 'Lorem ipsum dolor sit amet',
                'dob' => '2019-09-01',
                'country_of_birth' => 'Lorem ipsum dolor sit amet',
                'if_research_participant' => 'Lorem ipsum dolor sit amet',
                'deceased_year' => 'Lo',
                'annual_income' => 'Lorem ipsum dolor sit amet',
                'preferred_language' => 'Lorem ipsum dolor sit amet',
                'is_indigenous_or_islander' => 'Lorem ipsum dolor sit amet',
                'date_captured' => '2019-09-01',
                'deleted' => 1,
                'post_code' => 'Lo'
            ],
        ];
        parent::init();
    }
}
