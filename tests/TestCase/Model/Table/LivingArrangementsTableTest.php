<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LivingArrangementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LivingArrangementsTable Test Case
 */
class LivingArrangementsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LivingArrangementsTable
     */
    public $LivingArrangements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.LivingArrangements',
        'app.Patients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LivingArrangements') ? [] : ['className' => LivingArrangementsTable::class];
        $this->LivingArrangements = TableRegistry::getTableLocator()->get('LivingArrangements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LivingArrangements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
