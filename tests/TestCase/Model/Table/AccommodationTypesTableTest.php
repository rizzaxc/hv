<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccommodationTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccommodationTypesTable Test Case
 */
class AccommodationTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AccommodationTypesTable
     */
    public $AccommodationTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AccommodationTypes',
        'app.Patients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AccommodationTypes') ? [] : ['className' => AccommodationTypesTable::class];
        $this->AccommodationTypes = TableRegistry::getTableLocator()->get('AccommodationTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccommodationTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
