<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DeathCausesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DeathCausesTable Test Case
 */
class DeathCausesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DeathCausesTable
     */
    public $DeathCauses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DeathCauses',
        'app.Patients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DeathCauses') ? [] : ['className' => DeathCausesTable::class];
        $this->DeathCauses = TableRegistry::getTableLocator()->get('DeathCauses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DeathCauses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
