<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HuntingtonInheritancesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HuntingtonInheritancesTable Test Case
 */
class HuntingtonInheritancesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HuntingtonInheritancesTable
     */
    public $HuntingtonInheritances;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HuntingtonInheritances',
        'app.Patients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HuntingtonInheritances') ? [] : ['className' => HuntingtonInheritancesTable::class];
        $this->HuntingtonInheritances = TableRegistry::getTableLocator()->get('HuntingtonInheritances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HuntingtonInheritances);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
