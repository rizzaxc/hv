<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ManagingEntitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ManagingEntitiesTable Test Case
 */
class ManagingEntitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ManagingEntitiesTable
     */
    public $ManagingEntities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ManagingEntities',
        'app.AccountRequestInfo',
        'app.Staff'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ManagingEntities') ? [] : ['className' => ManagingEntitiesTable::class];
        $this->ManagingEntities = TableRegistry::getTableLocator()->get('ManagingEntities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ManagingEntities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
