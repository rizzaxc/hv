<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OtherConditionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OtherConditionsTable Test Case
 */
class OtherConditionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OtherConditionsTable
     */
    public $OtherConditions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OtherConditions',
        'app.Patients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OtherConditions') ? [] : ['className' => OtherConditionsTable::class];
        $this->OtherConditions = TableRegistry::getTableLocator()->get('OtherConditions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OtherConditions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
