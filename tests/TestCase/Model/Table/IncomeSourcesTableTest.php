<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IncomeSourcesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IncomeSourcesTable Test Case
 */
class IncomeSourcesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\IncomeSourcesTable
     */
    public $IncomeSources;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.IncomeSources',
        'app.Patients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('IncomeSources') ? [] : ['className' => IncomeSourcesTable::class];
        $this->IncomeSources = TableRegistry::getTableLocator()->get('IncomeSources', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IncomeSources);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
