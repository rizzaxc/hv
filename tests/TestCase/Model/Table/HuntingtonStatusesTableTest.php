<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HuntingtonStatusesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HuntingtonStatusesTable Test Case
 */
class HuntingtonStatusesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HuntingtonStatusesTable
     */
    public $HuntingtonStatuses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HuntingtonStatuses',
        'app.Patients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HuntingtonStatuses') ? [] : ['className' => HuntingtonStatusesTable::class];
        $this->HuntingtonStatuses = TableRegistry::getTableLocator()->get('HuntingtonStatuses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HuntingtonStatuses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
