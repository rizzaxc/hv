-- SET FOREIGN_KEY_CHECKS = 0;

-- DROP TABLE IF EXISTS `huntington_symptoms_patients`;
-- DROP TABLE IF EXISTS `huntington_symptoms`;
-- DROP TABLE IF EXISTS `income_sources_patients`;
-- DROP TABLE IF EXISTS `income_sources`;
-- DROP TABLE IF EXISTS `managing_entities`;
-- DROP TABLE IF EXISTS `staff`;
-- DROP TABLE IF EXISTS `patients`;
-- DROP TABLE IF EXISTS `other_conditions_patients`;
-- DROP TABLE IF EXISTS `other_conditions`;
-- DROP TABLE IF EXISTS `accommodation_types`;
-- DROP TABLE IF EXISTS `account_request_info`;
-- DROP TABLE IF EXISTS `death_causes`;
-- DROP TABLE IF EXISTS `employment_types`;
-- DROP TABLE IF EXISTS `genders`;
-- DROP TABLE IF EXISTS `huntington_inheritances`;
-- DROP TABLE IF EXISTS `huntington_statuses`;
-- DROP TABLE IF EXISTS `living_arrangements`;
-- DROP TABLE IF EXISTS `marital_statuses`;

-- SET FOREIGN_KEY_CHECKS = 1;


create table accommodation_types
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table account_request_info
(
	id int auto_increment
		primary key,
	code varchar(255) not null,
	timestamp datetime not null,
	email varchar(255) not null,
    managing_entity_id int not null,
    constraint account_request_info_ibfk_1
		foreign key (managing_entity_id) references managing_entities (id)
)
;

create table death_causes
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table employment_types
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table genders
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table huntington_inheritances
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table huntington_statuses
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table income_sources
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table living_arrangements
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table managing_entities
(
	id int auto_increment
		primary key,
	name varchar(255) not null,
	post_code varchar(4) not null
)
;

create table marital_statuses
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table other_conditions
(
	id int auto_increment
		primary key,
	description varchar(255) null
)
;

create table staff
(
	id int auto_increment
		primary key,
	managing_entity_id int null,
	role varchar(255) null,
	first_name varchar(255) not null,
	last_name varchar(255) not null,
	middle_name varchar(255) null,
	password varchar(255) null,
	username varchar(64) not null,
	phone_number varchar(255) null,
	password_timestamp datetime default CURRENT_TIMESTAMP not null,
	token varchar(255) null,
	constraint staff_username_uindex
		unique (username),
	constraint staff_ibfk_1
		foreign key (managing_entity_id) references managing_entities (id)
)
;

create table patients
(
	id int auto_increment
		primary key,
	staff_id int null,
	gender_id int not null,
	marital_status_id int not null,
	huntington_status_id int null,
	accommodation_type_id int not null,
	employment_type_id int not null,
	huntington_inheritance_id int null,
	living_arrangement_id int not null,
	death_cause_id int null,
	first_name varchar(255) null,
	middle_name varchar(255) null,
	last_name varchar(255) null,
	dob date not null,
	country_of_birth varchar(255) not null,
	if_research_participant varchar(255) not null,
	deceased_year varchar(4) null,
	annual_income varchar(255) null,
	preferred_language varchar(255) null,
	is_indigenous_or_islander varchar(255) not null,
	date_captured date not null,
	deleted tinyint(1) default '0' not null,
	post_code varchar(4) null,
	constraint patients_ibfk_1
		foreign key (staff_id) references staff (id),
	constraint patients_ibfk_2
		foreign key (gender_id) references genders (id),
	constraint patients_ibfk_3
		foreign key (marital_status_id) references marital_statuses (id),
	constraint patients_ibfk_4
		foreign key (huntington_status_id) references huntington_statuses (id),
	constraint patients_ibfk_5
		foreign key (accommodation_type_id) references accommodation_types (id),
	constraint patients_ibfk_6
		foreign key (employment_type_id) references employment_types (id),
	constraint patients_ibfk_7
		foreign key (huntington_inheritance_id) references huntington_inheritances (id),
	constraint patients_ibfk_8
		foreign key (living_arrangement_id) references living_arrangements (id),
	constraint patients_ibfk_9
		foreign key (death_cause_id) references death_causes (id)
)
;

create table income_sources_patients
(
	id int auto_increment
		primary key,
	patient_id int null,
	income_source_id int null,
	constraint income_sources_patients_ibfk_1
		foreign key (patient_id) references patients (id),
	constraint income_sources_patients_ibfk_2
		foreign key (income_source_id) references income_sources (id)
)
;

create index income_source_id
	on income_sources_patients (income_source_id)
;

create index patient_id
	on income_sources_patients (patient_id)
;

create table other_conditions_patients
(
	id int auto_increment
		primary key,
	patient_id int null,
	other_condition_id int null,
	year_diagnosed date null,
	constraint other_conditions_patients_ibfk_1
		foreign key (patient_id) references patients (id),
	constraint other_conditions_patients_ibfk_2
		foreign key (other_condition_id) references other_conditions (id)
)
;

create index other_condition_id
	on other_conditions_patients (other_condition_id)
;

create index patient_id
	on other_conditions_patients (patient_id)
;

create index accommodation_type_id
	on patients (accommodation_type_id)
;

create index death_cause_id
	on patients (death_cause_id)
;

create index employment_type_id
	on patients (employment_type_id)
;

create index gender_id
	on patients (gender_id)
;

create index huntington_inheritance_id
	on patients (huntington_inheritance_id)
;

create index huntington_status_id
	on patients (huntington_status_id)
;

create index living_arrangement_id
	on patients (living_arrangement_id)
;

create index marital_status_id
	on patients (marital_status_id)
;

create index staff_id
	on patients (staff_id)
;

create index managing_entity_id
	on staff (managing_entity_id)
;

create index managing_entity_id
	on account_request_info (managing_entity_id)
;