<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Staff Entity
 *
 * @property int $id
 * @property int|null $managing_entity_id
 * @property string|null $role
 * @property string $first_name
 * @property string $last_name
 * @property string|null $middle_name
 * @property string|null $password
 * @property string $username
 * @property string|null $phone_number
 * @property \Cake\I18n\FrozenTime $password_timestamp
 * @property string|null $token
 *
 * @property \App\Model\Entity\ManagingEntity $managing_entity
 * @property \App\Model\Entity\Patient[] $patients
 */
class Staff extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'managing_entity_id' => true,
        'role' => true,
        'first_name' => true,
        'last_name' => true,
        'middle_name' => true,
        'password' => true,
        'username' => true,
        'phone_number' => true,
        'password_timestamp' => true,
        'token' => true,
        'managing_entity' => true,
        'patients' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token'
    ];
}
