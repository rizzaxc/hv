<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Patient Entity
 *
 * @property int $id
 * @property int|null $staff_id
 * @property int $gender_id
 * @property int $marital_status_id
 * @property int|null $huntington_status_id
 * @property int $accommodation_type_id
 * @property int $employment_type_id
 * @property int|null $huntington_inheritance_id
 * @property int $living_arrangement_id
 * @property int|null $death_cause_id
 * @property string|null $first_name
 * @property string|null $middle_name
 * @property string|null $last_name
 * @property \Cake\I18n\FrozenDate $dob
 * @property string $country_of_birth
 * @property string $if_research_participant
 * @property string|null $deceased_year
 * @property string|null $annual_income
 * @property string|null $preferred_language
 * @property string $is_indigenous_or_islander
 * @property \Cake\I18n\FrozenDate $date_captured
 * @property bool $deleted
 * @property string|null $post_code
 *
 * @property \App\Model\Entity\Staff $staff
 * @property \App\Model\Entity\Gender $gender
 * @property \App\Model\Entity\MaritalStatus $marital_status
 * @property \App\Model\Entity\HuntingtonStatus $huntington_status
 * @property \App\Model\Entity\AccommodationType $accommodation_type
 * @property \App\Model\Entity\EmploymentType $employment_type
 * @property \App\Model\Entity\HuntingtonInheritance $huntington_inheritance
 * @property \App\Model\Entity\LivingArrangement $living_arrangement
 * @property \App\Model\Entity\DeathCause $death_cause
 * @property \App\Model\Entity\IncomeSource[] $income_sources
 * @property \App\Model\Entity\OtherCondition[] $other_conditions
 */
class Patient extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'staff_id' => true,
        'gender_id' => true,
        'marital_status_id' => true,
        'huntington_status_id' => true,
        'accommodation_type_id' => true,
        'employment_type_id' => true,
        'huntington_inheritance_id' => true,
        'living_arrangement_id' => true,
        'death_cause_id' => true,
        'first_name' => true,
        'middle_name' => true,
        'last_name' => true,
        'dob' => true,
        'country_of_birth' => true,
        'if_research_participant' => true,
        'deceased_year' => true,
        'annual_income' => true,
        'preferred_language' => true,
        'is_indigenous_or_islander' => true,
        'date_captured' => true,
        'deleted' => true,
        'post_code' => true,
        'staff' => true,
        'gender' => true,
        'marital_status' => true,
        'huntington_status' => true,
        'accommodation_type' => true,
        'employment_type' => true,
        'huntington_inheritance' => true,
        'living_arrangement' => true,
        'death_cause' => true,
        'income_sources' => true,
        'other_conditions' => true
    ];
}
