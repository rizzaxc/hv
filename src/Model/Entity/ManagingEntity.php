<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ManagingEntity Entity
 *
 * @property int $id
 * @property string $name
 * @property string $post_code
 *
 * @property \App\Model\Entity\AccountRequestInfo[] $account_request_info
 * @property \App\Model\Entity\Staff[] $staff
 */
class ManagingEntity extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'post_code' => true,
        'account_request_info' => true,
        'staff' => true
    ];
}
