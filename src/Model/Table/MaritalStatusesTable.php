<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;



/**
 * MaritalStatuses Model
 *
 * @property \App\Model\Table\PatientsTable|\Cake\ORM\Association\HasMany $Patients
 *
 * @method \App\Model\Entity\MaritalStatus get($primaryKey, $options = [])
 * @method \App\Model\Entity\MaritalStatus newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MaritalStatus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MaritalStatus|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaritalStatus saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MaritalStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MaritalStatus[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MaritalStatus findOrCreate($search, callable $callback = null, $options = [])
 */
class MaritalStatusesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('marital_statuses');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->hasMany('Patients', [
            'foreignKey' => 'marital_status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->allowEmptyString('status');

        return $validator;
    }
}
