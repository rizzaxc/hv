<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AccommodationTypes Model
 *
 * @property \App\Model\Table\PatientsTable&\Cake\ORM\Association\HasMany $Patients
 *
 * @method \App\Model\Entity\AccommodationType get($primaryKey, $options = [])
 * @method \App\Model\Entity\AccommodationType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AccommodationType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AccommodationType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccommodationType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccommodationType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AccommodationType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AccommodationType findOrCreate($search, callable $callback = null, $options = [])
 */
class AccommodationTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accommodation_types');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->hasMany('Patients', [
            'foreignKey' => 'accommodation_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        return $validator;
    }
}
