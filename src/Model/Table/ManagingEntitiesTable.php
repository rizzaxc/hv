<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ManagingEntities Model
 *
 * @property \App\Model\Table\AccountRequestInfoTable&\Cake\ORM\Association\HasMany $AccountRequestInfo
 * @property \App\Model\Table\StaffTable&\Cake\ORM\Association\HasMany $Staff
 *
 * @method \App\Model\Entity\ManagingEntity get($primaryKey, $options = [])
 * @method \App\Model\Entity\ManagingEntity newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ManagingEntity[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ManagingEntity|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ManagingEntity saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ManagingEntity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ManagingEntity[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ManagingEntity findOrCreate($search, callable $callback = null, $options = [])
 */
class ManagingEntitiesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('managing_entities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('AccountRequestInfo', [
            'foreignKey' => 'managing_entity_id'
        ]);
        $this->hasMany('Staff', [
            'foreignKey' => 'managing_entity_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('post_code')
            ->maxLength('post_code', 4)
            ->requirePresence('post_code', 'create')
            ->notEmptyString('post_code');

        return $validator;
    }
}
