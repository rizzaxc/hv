<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IncomeSources Model
 *
 * @property \App\Model\Table\PatientsTable&\Cake\ORM\Association\BelongsToMany $Patients
 *
 * @method \App\Model\Entity\IncomeSource get($primaryKey, $options = [])
 * @method \App\Model\Entity\IncomeSource newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\IncomeSource[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\IncomeSource|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\IncomeSource saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\IncomeSource patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\IncomeSource[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\IncomeSource findOrCreate($search, callable $callback = null, $options = [])
 */
class IncomeSourcesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('income_sources');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Patients', [
            'foreignKey' => 'income_source_id',
            'targetForeignKey' => 'patient_id',
            'joinTable' => 'income_sources_patients'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        return $validator;
    }
}
