<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Patients Model
 *
 * @property \App\Model\Table\StaffTable&\Cake\ORM\Association\BelongsTo $Staff
 * @property \App\Model\Table\GendersTable&\Cake\ORM\Association\BelongsTo $Genders
 * @property \App\Model\Table\MaritalStatusesTable&\Cake\ORM\Association\BelongsTo $MaritalStatuses
 * @property \App\Model\Table\HuntingtonStatusesTable&\Cake\ORM\Association\BelongsTo $HuntingtonStatuses
 * @property \App\Model\Table\AccommodationTypesTable&\Cake\ORM\Association\BelongsTo $AccommodationTypes
 * @property \App\Model\Table\EmploymentTypesTable&\Cake\ORM\Association\BelongsTo $EmploymentTypes
 * @property \App\Model\Table\HuntingtonInheritancesTable&\Cake\ORM\Association\BelongsTo $HuntingtonInheritances
 * @property \App\Model\Table\LivingArrangementsTable&\Cake\ORM\Association\BelongsTo $LivingArrangements
 * @property \App\Model\Table\DeathCausesTable&\Cake\ORM\Association\BelongsTo $DeathCauses
 * @property \App\Model\Table\IncomeSourcesTable&\Cake\ORM\Association\BelongsToMany $IncomeSources
 * @property \App\Model\Table\OtherConditionsTable&\Cake\ORM\Association\BelongsToMany $OtherConditions
 *
 * @method \App\Model\Entity\Patient get($primaryKey, $options = [])
 * @method \App\Model\Entity\Patient newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Patient[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Patient|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Patient saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Patient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Patient[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Patient findOrCreate($search, callable $callback = null, $options = [])
 */
class PatientsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('patients');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Staff', [
            'foreignKey' => 'staff_id'
        ]);
        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MaritalStatuses', [
            'foreignKey' => 'marital_status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('HuntingtonStatuses', [
            'foreignKey' => 'huntington_status_id'
        ]);
        $this->belongsTo('AccommodationTypes', [
            'foreignKey' => 'accommodation_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('EmploymentTypes', [
            'foreignKey' => 'employment_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('HuntingtonInheritances', [
            'foreignKey' => 'huntington_inheritance_id'
        ]);
        $this->belongsTo('LivingArrangements', [
            'foreignKey' => 'living_arrangement_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DeathCauses', [
            'foreignKey' => 'death_cause_id'
        ]);
        $this->belongsToMany('IncomeSources', [
            'foreignKey' => 'patient_id',
            'targetForeignKey' => 'income_source_id',
            'joinTable' => 'income_sources_patients'
        ]);
        $this->belongsToMany('OtherConditions', [
            'foreignKey' => 'patient_id',
            'targetForeignKey' => 'other_condition_id',
            'joinTable' => 'other_conditions_patients'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->allowEmptyString('first_name');

        $validator
            ->scalar('middle_name')
            ->maxLength('middle_name', 255)
            ->allowEmptyString('middle_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->allowEmptyString('last_name');

        $validator
            ->date('dob')
            ->requirePresence('dob', 'create')
            ->notEmptyDate('dob');

        $validator
            ->scalar('country_of_birth')
            ->maxLength('country_of_birth', 255)
            ->requirePresence('country_of_birth', 'create')
            ->notEmptyString('country_of_birth');

        $validator
            ->scalar('if_research_participant')
            ->maxLength('if_research_participant', 255)
            ->requirePresence('if_research_participant', 'create')
            ->notEmptyString('if_research_participant');

        $validator
            ->scalar('deceased_year')
            ->maxLength('deceased_year', 4)
            ->allowEmptyString('deceased_year');

        $validator
            ->scalar('annual_income')
            ->maxLength('annual_income', 255)
            ->allowEmptyString('annual_income');

        $validator
            ->scalar('preferred_language')
            ->maxLength('preferred_language', 255)
            ->allowEmptyString('preferred_language');

        $validator
            ->scalar('is_indigenous_or_islander')
            ->maxLength('is_indigenous_or_islander', 255)
            ->requirePresence('is_indigenous_or_islander', 'create')
            ->notEmptyString('is_indigenous_or_islander');

        $validator
            ->date('date_captured')
            ->requirePresence('date_captured', 'create')
            ->notEmptyDate('date_captured');

        $validator
            ->boolean('deleted')
            ->notEmptyString('deleted');

        $validator
            ->scalar('post_code')
            ->maxLength('post_code', 4)
            ->allowEmptyString('post_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['staff_id'], 'Staff'));
        $rules->add($rules->existsIn(['gender_id'], 'Genders'));
        $rules->add($rules->existsIn(['marital_status_id'], 'MaritalStatuses'));
        $rules->add($rules->existsIn(['huntington_status_id'], 'HuntingtonStatuses'));
        $rules->add($rules->existsIn(['accommodation_type_id'], 'AccommodationTypes'));
        $rules->add($rules->existsIn(['employment_type_id'], 'EmploymentTypes'));
        $rules->add($rules->existsIn(['huntington_inheritance_id'], 'HuntingtonInheritances'));
        $rules->add($rules->existsIn(['living_arrangement_id'], 'LivingArrangements'));
        $rules->add($rules->existsIn(['death_cause_id'], 'DeathCauses'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // Format the 2 fields to the correct mysql format
        if (isset($data['dob'])) {
            $new = date('Y-m-d',strtotime($data['dob']));
            $data['dob'] = $new;
        }

        if (isset($data['date_captured'])) {
            $new = date('Y-m-d',strtotime($data['date_captured']));
            $data['date_captured'] = $new;
        }
    }
}
