<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OtherConditions Model
 *
 * @property \App\Model\Table\PatientsTable&\Cake\ORM\Association\BelongsToMany $Patients
 *
 * @method \App\Model\Entity\OtherCondition get($primaryKey, $options = [])
 * @method \App\Model\Entity\OtherCondition newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OtherCondition[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OtherCondition|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OtherCondition saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OtherCondition patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OtherCondition[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OtherCondition findOrCreate($search, callable $callback = null, $options = [])
 */
class OtherConditionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('other_conditions');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Patients', [
            'foreignKey' => 'other_condition_id',
            'targetForeignKey' => 'patient_id',
            'joinTable' => 'other_conditions_patients'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        return $validator;
    }
}
