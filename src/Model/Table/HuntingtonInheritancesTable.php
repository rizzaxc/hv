<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HuntingtonInheritances Model
 *
 * @property \App\Model\Table\PatientsTable&\Cake\ORM\Association\HasMany $Patients
 *
 * @method \App\Model\Entity\HuntingtonInheritance get($primaryKey, $options = [])
 * @method \App\Model\Entity\HuntingtonInheritance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HuntingtonInheritance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HuntingtonInheritance|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HuntingtonInheritance saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HuntingtonInheritance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HuntingtonInheritance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HuntingtonInheritance findOrCreate($search, callable $callback = null, $options = [])
 */
class HuntingtonInheritancesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('huntington_inheritances');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->hasMany('Patients', [
            'foreignKey' => 'huntington_inheritance_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        return $validator;
    }
}
