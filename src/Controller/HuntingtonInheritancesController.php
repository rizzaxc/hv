<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HuntingtonInheritances Controller
 *
 * @property \App\Model\Table\HuntingtonInheritancesTable $HuntingtonInheritances
 *
 * @method \App\Model\Entity\HuntingtonInheritance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HuntingtonInheritancesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $huntingtonInheritances = $this->paginate($this->HuntingtonInheritances);

        $this->set(compact('huntingtonInheritances'));
    }

    /**
     * View method
     *
     * @param string|null $id Huntington Inheritance id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $huntingtonInheritance = $this->HuntingtonInheritances->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('huntingtonInheritance', $huntingtonInheritance);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $huntingtonInheritance = $this->HuntingtonInheritances->newEntity();
        if ($this->request->is('post')) {
            $huntingtonInheritance = $this->HuntingtonInheritances->patchEntity($huntingtonInheritance, $this->request->getData());
            if ($this->HuntingtonInheritances->save($huntingtonInheritance)) {
                $this->Flash->success(__('The huntington inheritance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The huntington inheritance could not be saved. Please, try again.'));
        }
        $this->set(compact('huntingtonInheritance'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Huntington Inheritance id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $huntingtonInheritance = $this->HuntingtonInheritances->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $huntingtonInheritance = $this->HuntingtonInheritances->patchEntity($huntingtonInheritance, $this->request->getData());
            if ($this->HuntingtonInheritances->save($huntingtonInheritance)) {
                $this->Flash->success(__('The huntington inheritance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The huntington inheritance could not be saved. Please, try again.'));
        }
        $this->set(compact('huntingtonInheritance'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Huntington Inheritance id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $huntingtonInheritance = $this->HuntingtonInheritances->get($id);
        if ($this->HuntingtonInheritances->delete($huntingtonInheritance)) {
            $this->Flash->success(__('The huntington inheritance has been deleted.'));
        } else {
            $this->Flash->error(__('The huntington inheritance could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
