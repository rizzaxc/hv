<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ManagingEntities Controller
 *
 * @property \App\Model\Table\ManagingEntitiesTable $ManagingEntities
 *
 * @method \App\Model\Entity\ManagingEntity[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ManagingEntitiesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $managingEntities = $this->paginate($this->ManagingEntities);

        $this->set(compact('managingEntities'));
    }

    /**
     * View method
     *
     * @param string|null $id Managing Entity id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $managingEntity = $this->ManagingEntities->get($id, [
            'contain' => ['Staff']
        ]);

        $this->set('managingEntity', $managingEntity);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $managingEntity = $this->ManagingEntities->newEntity();
        if ($this->request->is('post')) {
            $managingEntity = $this->ManagingEntities->patchEntity($managingEntity, $this->request->getData());
            if ($this->ManagingEntities->save($managingEntity)) {
                $this->Flash->success(__('The managing entity has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The managing entity could not be saved. Please, try again.'));
        }
        $this->set(compact('managingEntity'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Managing Entity id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $managingEntity = $this->ManagingEntities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $managingEntity = $this->ManagingEntities->patchEntity($managingEntity, $this->request->getData());
            if ($this->ManagingEntities->save($managingEntity)) {
                $this->Flash->success(__('The managing entity has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The managing entity could not be saved. Please, try again.'));
        }
        $this->set(compact('managingEntity'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Managing Entity id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $managingEntity = $this->ManagingEntities->get($id);
        if ($this->ManagingEntities->delete($managingEntity)) {
            $this->Flash->success(__('The managing entity has been deleted.'));
        } else {
            $this->Flash->error(__('The managing entity could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
