<?php
namespace App\Controller;
use Cake\Datasource\ConnectionManager;

class DashboardController extends AppController
{
    public function isAuthorized($staff)
    {
        // All registered users can add articles
        // Prior to 3.4.0 $this->request->param('action') was used.
        if ($this->Auth->user('role') == 'pending'){
            return false;
        }
        if ($this->request->getParam('action') === 'index') {
            return true;
        }

        return parent::isAuthorized($staff);
    }
    public function index()
    {
        $connection = ConnectionManager::get('default');
        $pending = $connection
            ->newQuery()
            ->select('id')
            ->from('staff')
            ->where(['role' => 'pending'])
            ->rowCountAndClose();

        $this->set('pending', $pending);

    }


}
