<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        //Change this part according to Kate on the forum
        $this->loadComponent('RequestHandler',['enableBeforeRedirect' => false]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form'=>[
                    'userModel' => 'Staff',
                    'fields'=>[
                        'username'=>'username',
                        'password'=>'password'
                    ]
                ]
            ],
            'loginAction'=>[
                'controller'=>'Staff',
                'action'=>'login'
            ],

            'loginRedirect' => [
                'controller' => 'Dashboard',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Staff',
                'action' => 'login',

            ]
        ]);

        if($this->Auth->user('role') == 'admin'||"clinic"||"staff")
        {
            $this->layout = 'default';

        }
        else
        {
            $this->layout = 'homelayout';
        }
        if($this->Auth->user('role')=='pending'){
            $this->Flash->error(__('You are not allow to login now, please wait for verified.'));
            $this->redirect(['controller'=>'staff','action' => 'logout']);

        }
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }
    public function isAuthorized($staff)
    {
        // Admin can access every action
        if (isset($staff['role']) && $staff['role'] === 'admin') {
            return true;
        }
        else {

            // Default deny
            return false;
        }
    }
    public function beforeFilter(Event $event )
    {
        $role=$this->Auth->user('role');
        if ($role == 'clinic' ||$role=='staff'|| $role == 'admin') {
            $this->set('role', $role);
        }

        if($role == 'admin')
        {
            $this->set('is_admin', true);
        }
        else
        {
            $this->set('is_admin', false);;
        }
        if($role == 'staff')
        {
            $this->set('is_staff', true);

        }
        else
        {
            $this->set('is_staff', false);
        }
        if($role=='clinic'){
            $this->set('is_clinic',true);

        }
        else{
            $this->set('is_clinic',false);
        }
        $this->Auth->allow(['logout','register' ]);
    }

}

