<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Patients Controller
 *
 * @property \App\Model\Table\PatientsTable $Patients
 *
 * @method \App\Model\Entity\Patient[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PatientsController extends AppController
{
    public function isAuthorized($staff)
    {
        // All registered users can add articles
        // Prior to 3.4.0 $this->request->param('action') was used.
        if ($this->request->getParam('action') === 'index'||'add') {
            return true;
        }

        // The owner of an article can edit and delete i


        return parent::isAuthorized($staff);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [ 'limit'=>99999999,'maxLimit'=>99999999,
            'contain' => ['Staff', 'Genders', 'MaritalStatuses', 'HuntingtonStatuses', 'AccommodationTypes', 'EmploymentTypes', 'HuntingtonInheritances', 'LivingArrangements', 'DeathCauses']
        ];
        $patients = $this->paginate($this->Patients->find('all')->where(['deleted'=>'0']));

        $this->set(compact('patients'));
    }
    
    public function recycle()
    {
        if ($this->Auth->user('role')=='admin') {
            $this->paginate = ['limit' => 99999999, 'maxLimit' => 99999999,
                'contain' => ['Staff', 'Genders', 'MaritalStatuses', 'HuntingtonStatuses', 'AccommodationTypes', 'EmploymentTypes', 'HuntingtonInheritances', 'LivingArrangements', 'DeathCauses']
            ];
            $patients = $this->paginate($this->Patients->find('all')->where(['deleted' => '1']));

            $this->set(compact('patients'));
        }else{
            $this->Flash->error(__('You are not authorized to access this location.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Patient id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $patient = $this->Patients->get($id, [
            'contain' => ['Staff', 'Genders', 'MaritalStatuses', 'HuntingtonStatuses', 'AccommodationTypes', 'EmploymentTypes', 'HuntingtonInheritances', 'LivingArrangements', 'DeathCauses', 'IncomeSources', 'OtherConditions']
        ]);

        $this->set('patient', $patient);
    }

    public function ownpatient()
    {

        $this->paginate = ['limit'=>99999999,'maxLimit'=>99999999,
            'conditions'=>['staff_id' =>$this->Auth->user('id') ] ,
            'contain' => ['Staff']
        ];

        $patients = $this->paginate($this->Patients);

        $this->set(compact('patients'));
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $patient = $this->Patients->newEntity();
        if ($this->request->is('post')) {
            $patient = $this->Patients->patchEntity($patient, $this->request->getData());
            if ($this->Patients->save($patient)) {
                $this->Flash->success(__('The patient has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Invalid input'));
        }
        $staff = $this->Auth->user('id');
        $genders = $this->Patients->Genders->find('list', ['limit' => 200]);
        $maritalStatuses = $this->Patients->MaritalStatuses->find('list', ['limit' => 200]);
        $huntingtonStatuses = $this->Patients->HuntingtonStatuses->find('list', ['limit' => 200]);
        $accommodationTypes = $this->Patients->AccommodationTypes->find('list', ['limit' => 200]);
        $employmentTypes = $this->Patients->EmploymentTypes->find('list', ['limit' => 200]);
        $huntingtonInheritances = $this->Patients->HuntingtonInheritances->find('list', ['limit' => 200]);
        $livingArrangements = $this->Patients->LivingArrangements->find('list', ['limit' => 200]);
        $deathCauses = $this->Patients->DeathCauses->find('list', ['limit' => 200]);
        $incomeSources = $this->Patients->IncomeSources->find('list', ['limit' => 200]);
        $otherConditions = $this->Patients->OtherConditions->find('list', ['limit' => 200]);
        $this->set(compact('patient', 'staff', 'genders', 'maritalStatuses', 'huntingtonStatuses', 'accommodationTypes', 'employmentTypes', 'huntingtonInheritances', 'livingArrangements', 'deathCauses', 'incomeSources', 'otherConditions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Patient id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $patient = $this->Patients->get($id, [
            'contain' => ['IncomeSources', 'OtherConditions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $patient = $this->Patients->patchEntity($patient, $this->request->getData());
            if ($this->Patients->save($patient)) {
                $this->Flash->success(__('The patient has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The patient could not be saved. Please, try again.'));
        }
        $staff = $this->Patients->Staff->find('list', ['limit' => 200]);
        $genders = $this->Patients->Genders->find('list', ['limit' => 200]);
        $maritalStatuses = $this->Patients->MaritalStatuses->find('list', ['limit' => 200]);
        $huntingtonStatuses = $this->Patients->HuntingtonStatuses->find('list', ['limit' => 200]);
        $accommodationTypes = $this->Patients->AccommodationTypes->find('list', ['limit' => 200]);
        $employmentTypes = $this->Patients->EmploymentTypes->find('list', ['limit' => 200]);
        $huntingtonInheritances = $this->Patients->HuntingtonInheritances->find('list', ['limit' => 200]);
        $livingArrangements = $this->Patients->LivingArrangements->find('list', ['limit' => 200]);
        $deathCauses = $this->Patients->DeathCauses->find('list', ['limit' => 200]);
        $incomeSources = $this->Patients->IncomeSources->find('list', ['limit' => 200]);
        $otherConditions = $this->Patients->OtherConditions->find('list', ['limit' => 200]);
        $this->set(compact('patient', 'staff', 'genders', 'maritalStatuses', 'huntingtonStatuses', 'accommodationTypes', 'employmentTypes', 'huntingtonInheritances', 'livingArrangements', 'deathCauses', 'incomeSources', 'otherConditions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Patient id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $patient = $this->Patients->get($id);
        $patient->deleted=1;
        if ($this->Patients->save($patient)) {
            $this->Flash->success(__('The patient has been deleted.'));
        } else {
            $this->Flash->error(__('The patient could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function recover($id = null)
    {
        $this->request->allowMethod(['post', 'recover']);
        $patient = $this->Patients->get($id);
        $patient->deleted=0;
        if ($this->Patients->save($patient)) {
            $this->Flash->success(__('The patient has been recovered.'));
        } else {
            $this->Flash->error(__('The patient could not be recovered. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
