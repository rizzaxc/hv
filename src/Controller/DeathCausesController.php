<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeathCauses Controller
 *
 * @property \App\Model\Table\DeathCausesTable $DeathCauses
 *
 * @method \App\Model\Entity\DeathCause[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeathCausesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $deathCauses = $this->paginate($this->DeathCauses);

        $this->set(compact('deathCauses'));
    }

    /**
     * View method
     *
     * @param string|null $id Death Cause id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $deathCause = $this->DeathCauses->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('deathCause', $deathCause);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deathCause = $this->DeathCauses->newEntity();
        if ($this->request->is('post')) {
            $deathCause = $this->DeathCauses->patchEntity($deathCause, $this->request->getData());
            if ($this->DeathCauses->save($deathCause)) {
                $this->Flash->success(__('The death cause has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The death cause could not be saved. Please, try again.'));
        }
        $this->set(compact('deathCause'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Death Cause id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $deathCause = $this->DeathCauses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $deathCause = $this->DeathCauses->patchEntity($deathCause, $this->request->getData());
            if ($this->DeathCauses->save($deathCause)) {
                $this->Flash->success(__('The death cause has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The death cause could not be saved. Please, try again.'));
        }
        $this->set(compact('deathCause'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Death Cause id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deathCause = $this->DeathCauses->get($id);
        if ($this->DeathCauses->delete($deathCause)) {
            $this->Flash->success(__('The death cause has been deleted.'));
        } else {
            $this->Flash->error(__('The death cause could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
