<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OtherConditions Controller
 *
 * @property \App\Model\Table\OtherConditionsTable $OtherConditions
 *
 * @method \App\Model\Entity\OtherCondition[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OtherConditionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $otherConditions = $this->paginate($this->OtherConditions);

        $this->set(compact('otherConditions'));
    }

    /**
     * View method
     *
     * @param string|null $id Other Condition id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $otherCondition = $this->OtherConditions->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('otherCondition', $otherCondition);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $otherCondition = $this->OtherConditions->newEntity();
        if ($this->request->is('post')) {
            $otherCondition = $this->OtherConditions->patchEntity($otherCondition, $this->request->getData());
            if ($this->OtherConditions->save($otherCondition)) {
                $this->Flash->success(__('The other condition has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The other condition could not be saved. Please, try again.'));
        }
        $patients = $this->OtherConditions->Patients->find('list', ['limit' => 200]);
        $this->set(compact('otherCondition', 'patients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Other Condition id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $otherCondition = $this->OtherConditions->get($id, [
            'contain' => ['Patients']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $otherCondition = $this->OtherConditions->patchEntity($otherCondition, $this->request->getData());
            if ($this->OtherConditions->save($otherCondition)) {
                $this->Flash->success(__('The other condition has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The other condition could not be saved. Please, try again.'));
        }
        $patients = $this->OtherConditions->Patients->find('list', ['limit' => 200]);
        $this->set(compact('otherCondition', 'patients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Other Condition id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $otherCondition = $this->OtherConditions->get($id);
        if ($this->OtherConditions->delete($otherCondition)) {
            $this->Flash->success(__('The other condition has been deleted.'));
        } else {
            $this->Flash->error(__('The other condition could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
