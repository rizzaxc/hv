<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LivingArrangements Controller
 *
 * @property \App\Model\Table\LivingArrangementsTable $LivingArrangements
 *
 * @method \App\Model\Entity\LivingArrangement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LivingArrangementsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $livingArrangements = $this->paginate($this->LivingArrangements);

        $this->set(compact('livingArrangements'));
    }

    /**
     * View method
     *
     * @param string|null $id Living Arrangement id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $livingArrangement = $this->LivingArrangements->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('livingArrangement', $livingArrangement);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $livingArrangement = $this->LivingArrangements->newEntity();
        if ($this->request->is('post')) {
            $livingArrangement = $this->LivingArrangements->patchEntity($livingArrangement, $this->request->getData());
            if ($this->LivingArrangements->save($livingArrangement)) {
                $this->Flash->success(__('The living arrangement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The living arrangement could not be saved. Please, try again.'));
        }
        $this->set(compact('livingArrangement'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Living Arrangement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $livingArrangement = $this->LivingArrangements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $livingArrangement = $this->LivingArrangements->patchEntity($livingArrangement, $this->request->getData());
            if ($this->LivingArrangements->save($livingArrangement)) {
                $this->Flash->success(__('The living arrangement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The living arrangement could not be saved. Please, try again.'));
        }
        $this->set(compact('livingArrangement'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Living Arrangement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $livingArrangement = $this->LivingArrangements->get($id);
        if ($this->LivingArrangements->delete($livingArrangement)) {
            $this->Flash->success(__('The living arrangement has been deleted.'));
        } else {
            $this->Flash->error(__('The living arrangement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
