<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EmploymentTypes Controller
 *
 * @property \App\Model\Table\EmploymentTypesTable $EmploymentTypes
 *
 * @method \App\Model\Entity\EmploymentType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmploymentTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $employmentTypes = $this->paginate($this->EmploymentTypes);

        $this->set(compact('employmentTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Employment Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employmentType = $this->EmploymentTypes->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('employmentType', $employmentType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $employmentType = $this->EmploymentTypes->newEntity();
        if ($this->request->is('post')) {
            $employmentType = $this->EmploymentTypes->patchEntity($employmentType, $this->request->getData());
            if ($this->EmploymentTypes->save($employmentType)) {
                $this->Flash->success(__('The employment type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employment type could not be saved. Please, try again.'));
        }
        $this->set(compact('employmentType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Employment Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $employmentType = $this->EmploymentTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employmentType = $this->EmploymentTypes->patchEntity($employmentType, $this->request->getData());
            if ($this->EmploymentTypes->save($employmentType)) {
                $this->Flash->success(__('The employment type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employment type could not be saved. Please, try again.'));
        }
        $this->set(compact('employmentType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Employment Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employmentType = $this->EmploymentTypes->get($id);
        if ($this->EmploymentTypes->delete($employmentType)) {
            $this->Flash->success(__('The employment type has been deleted.'));
        } else {
            $this->Flash->error(__('The employment type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
