<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HuntingtonStatuses Controller
 *
 * @property \App\Model\Table\HuntingtonStatusesTable $HuntingtonStatuses
 *
 * @method \App\Model\Entity\HuntingtonStatus[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HuntingtonStatusesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $huntingtonStatuses = $this->paginate($this->HuntingtonStatuses);

        $this->set(compact('huntingtonStatuses'));
    }

    /**
     * View method
     *
     * @param string|null $id Huntington Status id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $huntingtonStatus = $this->HuntingtonStatuses->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('huntingtonStatus', $huntingtonStatus);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $huntingtonStatus = $this->HuntingtonStatuses->newEntity();
        if ($this->request->is('post')) {
            $huntingtonStatus = $this->HuntingtonStatuses->patchEntity($huntingtonStatus, $this->request->getData());
            if ($this->HuntingtonStatuses->save($huntingtonStatus)) {
                $this->Flash->success(__('The huntington status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The huntington status could not be saved. Please, try again.'));
        }
        $this->set(compact('huntingtonStatus'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Huntington Status id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $huntingtonStatus = $this->HuntingtonStatuses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $huntingtonStatus = $this->HuntingtonStatuses->patchEntity($huntingtonStatus, $this->request->getData());
            if ($this->HuntingtonStatuses->save($huntingtonStatus)) {
                $this->Flash->success(__('The huntington status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The huntington status could not be saved. Please, try again.'));
        }
        $this->set(compact('huntingtonStatus'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Huntington Status id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $huntingtonStatus = $this->HuntingtonStatuses->get($id);
        if ($this->HuntingtonStatuses->delete($huntingtonStatus)) {
            $this->Flash->success(__('The huntington status has been deleted.'));
        } else {
            $this->Flash->error(__('The huntington status could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
