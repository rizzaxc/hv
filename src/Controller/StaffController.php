<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Utility\Security;
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
};
function timediff( $begin_time, $end_time )
{
    if ( $begin_time < $end_time ) {
        $starttime = $begin_time;
        $endtime = $end_time;
    } else {
        $starttime = $end_time;
        $endtime = $begin_time;
    }
    $timediff = $endtime - $starttime;
    $days = intval( $timediff / 86400 );


    return $days;
}
/**
 * Staff Controller
 *
 * @property \App\Model\Table\StaffTable $Staff
 *
 * @method \App\Model\Entity\Staff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */


class StaffController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $this->paginate = [
            'contain' => ['ManagingEntities']
        ];

        $staff = $this->paginate($this->Staff);

        $this->set(compact('staff'));
    }

    /**
     * View method
     *
     * @param string|null $id Staff id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $staff = $this->Staff->get($id, [
            'contain' => ['ManagingEntities', 'Patients']
        ]);

        $this->set('staff', $staff);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        $staff = $this->Staff->newEntity();

        if ($this->request->is('post')) {
            $mycode = $this->request->getData('code');
            $myemail = $this->request->getData('username');
            AppController::loadModel('accountRequestInfo');
            $this->loadModel('accountRequestInfo');
            //  $accountRequestInfoTable = TableRegistry::getTableLocator()->get('accountRequestInfo');
            if (
            $accountRequestInfo=($this->accountRequestInfo->find('all')->where(['code'=>$mycode])->first())){
                $timediff = timediff( strtotime($accountRequestInfo->timestamp), strtotime( Time::now()->i18nFormat('yyyy-MM-dd hh:mm:ss') ) );
                if($timediff>"3"){
                    $this->Flash->error(__('The code has expired.'));
                    $this->accountRequestInfo->delete($accountRequestInfo);
                }
                else{if ($accountRequestInfo->email==$myemail){
                    $staff = $this->Staff->patchEntity($staff, $this->request->getData());
                    if ($this->Staff->save($staff)) {
                        $this->Flash->success(__('The staff has been saved.'));
                        $this->accountRequestInfo->delete($accountRequestInfo);
                        return $this->redirect(['action' => 'login']);
                    }
                    $this->Flash->error(__('The staff could not be saved. Please, try again.'));
                }
                else{$this->Flash->error(__('email does not match'));}
                }
            }
            else{$this->Flash->error(__('code is wrong.'));}
        }

        $managingEntities = $this->Staff->ManagingEntities->find('list', ['limit' => 200]);
        $this->set(compact('staff', 'managingEntities'));
    }

    public function add()
    {
        $staff = $this->Staff->newEntity();
        if ($this->request->is('post')) {
            $staff = $this->Staff->patchEntity($staff, $this->request->getData());
            if ($this->Staff->save($staff)) {
                $this->Flash->success(__('The staff has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The staff could not be saved. Please, try again.'));
        }
        $managingEntities = $this->Staff->ManagingEntities->find('list', ['limit' => 200]);
        $this->set(compact('staff', 'managingEntities'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Staff id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $staff = $this->Staff->get($id, [
            'contain' => []
        ]);
        //  Logic for sending the email after verification
        $old_role = $staff->role;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $staff = $this->Staff->patchEntity($staff, $this->request->getData());
            if ($this->Staff->save($staff)) {
                $this->Flash->success(__('The staff has been saved.'));
                // if ($old_role == "pending" && $staff->role != $old_role) {
                //   $email = new Email('default');
                // $email->setTo($staff->username)
                //   ->setSubject("[Huntington's Victoria] You are verified")
                // ->send("You can now login the registry using your email and password");
                // }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The staff could not be saved. Please, try again.'));
        }
        $managingEntities = $this->Staff->ManagingEntities->find('list', ['limit' => 200]);
        $this->set(compact('staff', 'managingEntities'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Staff id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $staff = $this->Staff->get($id);
        if ($this->Staff->delete($staff)) {
            $this->Flash->success(__('The staff has been deleted.'));
        } else {
            $this->Flash->error(__('The staff could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['register', 'logout','resetpassword','forgetpassword','verification']);
    }

    public function login()
    {
        if ($this->Auth->user('id')) {
            $this->Flash->error(__('You are already logged in'));
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        if ($this->request->is('post')) {

            $staff = $this->Auth->identify();

            if ($staff) {
                $this->Auth->setUser($staff);
                $staff = $this->Staff->find('all')->where(['id'=>($this->Auth->user('id'))])->first();
                $timediff = timediff( strtotime( $staff->password_timestamp ), strtotime( Time::now()->i18nFormat('yyyy-MM-dd hh:mm:ss') ) );
                if ($staff->is_password_temporary=="1"){
                    $this->Flash->success(__('Because of the temporary password, please change the password first !'));
                    $this->redirect(['action' => 'resetpassword']);
                }elseif ($timediff>"180"){
                    $this->Flash->success(__('Over 180 days, please change the password first !'));
                    $this->redirect(['action' => 'resetpassword']);
                }
                if ($staff->role=='clinic'){
                    $this->redirect(['controller'=>'patients','action' => 'add']);
                }
                return $this->redirect($this->Auth->redirectUrl());

            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        $this->Flash->success(__('You are now logged out'));
        return $this->redirect($this->Auth->logout());
    }

    public function verifiedemail()
    {
        if ($this->request->is('post')) {
            Email::configTransport('testgmail', [
                'host' => 'smtp.gmail.com',
                'port' => 587,
                'username' => 'fit3048test@gmail.com',
                'password' => '6wR7v5QWEB.2M~D',
                'className' => 'Smtp',
                'tls' => true
            ]);

            $temporaryCode= generateRandomString();
            $myemail = $this->request->getData('Email');
            $accountRequestInfoTable = TableRegistry::get('accountRequestInfo');
            $accountRequestInfoTable =  TableRegistry::getTableLocator()->get('accountRequestInfo');
            $accountRequestInfo =$accountRequestInfoTable->newEntity();
            $accountRequestInfo->code=$temporaryCode;
            $accountRequestInfo->timestamp=Time::now()->i18nFormat('yyyy-MM-dd hh:mm:ss');
            $accountRequestInfo->email=$myemail;
            $accountRequestInfoTable->save($accountRequestInfo);
            $email = new Email('default');
            $email->transport('testgmail');
            $email->emailFormat('html');
            $email->from(['fit3048test@gmail.com' => 'Iteration 3'])
                ->to($myemail)
                ->subject('Verified email test')
                ->send('Hello '.$myemail.'<br/>This is the link to sign up your account: http://ie.infotech.monash.edu/team23/iteration4/staff/register
 <br/>This is your code : '.$temporaryCode.'');
            $this->Flash->success(__('Mail sent successfully'));
        }
    }
    public function forgetpassword()
    {

        if ($this->request->is(['patch', 'post', 'put'])) {
            Email::configTransport('testgmail', [
                'host' => 'smtp.gmail.com',
                'port' => 587,
                'username' => 'fit3048test@gmail.com',
                'password' => '6wR7v5QWEB.2M~D',
                'className' => 'Smtp',
                'tls' => true
            ]);

            $myemail = $this->request->getData('Email');

            $staff = $this->Staff->find('all')->where(['username' => $myemail])->first();

            if ($staff) {
                $myPhone = $this->request->getData('phone_number');
                if ($myPhone == $staff->phone_number) {

                   // $hasher=new DefaultPasswordHasher();

                    $mytoken = Security::hash(Security::randomBytes(32));
                    $staff->pend_password=$this->request->getData('password');
                    $staff->token = $mytoken;
                    $this->Staff->save($staff);
                    $email = new Email('default');
                    $email->transport('testgmail');
                    $email->emailFormat('html');
                    $email->from(['fit3048test@gmail.com' => 'Iteration 4'])
                        ->to($myemail)
                        ->subject('Forget Password test')
                        ->send('Hi ' . $myemail . '<br/>Please click the link to confirm your new password!<br/>http://ie.infotech.monash.edu/team23/iteration4/staff/verification/'. $mytoken );
                    $this->Flash->success(__('Mail sent successfully'));
                } else {
                    $this->Flash->error(__('The phone number does not match'));
                }
            } else {
                $this->Flash->error(__('The email does not exist'));
            }

        }


    }

    //   public function checkPassword($inputpassword,$staff)
    //   {
    //      return (new DefaultPasswordHasher)->check($inputpassword,$staff->password);
    //  }


    public function resetpassword(){
        $staff = $this->Staff->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $staff = $this->Staff->get($this->Auth->user('id'), [
                'contain' => []
            ]);
            $staff = $this->Staff->patchEntity($staff, $this->request->getData());

            //    $oldpassword=$this->request->getData('oldpassword');
//$hasher=new DefaultPasswordHasher();
//$oldpassword=$hasher->hash($this->request->getData('oldpassword'));
            $staff->password_timestamp = Time::now()->i18nFormat('yyyy-MM-dd hh:mm:ss');
            $staff->is_password_temporary = "0";
            if ($this->Staff->save($staff)) {
                $this->Flash->success(__('Password change successfully'));
                $this->redirect(['controller' => 'dashboard', 'action' => 'index']);
            } else {
                $this->Flash->error(__('Something is wrong'));
            }
        }
        $this->set(compact('staff'));
        $this->set('_serialize', ['staff']);
    }
    public  function verification($token){
        $staff=$this->Staff->find('all')->where(['token' => $token])->first();
        if($staff){
            $mypassword=$staff->pend_password;
            $staff->password=$mypassword;
            $staff->password_timestamp = Time::now()->i18nFormat('yyyy-MM-dd hh:mm:ss');
            $staff->pend_password=null;
            $staff->token=null;
            $this->Staff->save($staff);
        }else {
            return $this->redirect(['action' => 'index']);
        }
    }


}
