<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AccommodationTypes Controller
 *
 * @property \App\Model\Table\AccommodationTypesTable $AccommodationTypes
 *
 * @method \App\Model\Entity\AccommodationType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccommodationTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $accommodationTypes = $this->paginate($this->AccommodationTypes);

        $this->set(compact('accommodationTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Accommodation Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $accommodationType = $this->AccommodationTypes->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('accommodationType', $accommodationType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $accommodationType = $this->AccommodationTypes->newEntity();
        if ($this->request->is('post')) {
            $accommodationType = $this->AccommodationTypes->patchEntity($accommodationType, $this->request->getData());
            if ($this->AccommodationTypes->save($accommodationType)) {
                $this->Flash->success(__('The accommodation type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accommodation type could not be saved. Please, try again.'));
        }
        $this->set(compact('accommodationType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Accommodation Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $accommodationType = $this->AccommodationTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $accommodationType = $this->AccommodationTypes->patchEntity($accommodationType, $this->request->getData());
            if ($this->AccommodationTypes->save($accommodationType)) {
                $this->Flash->success(__('The accommodation type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accommodation type could not be saved. Please, try again.'));
        }
        $this->set(compact('accommodationType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Accommodation Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $accommodationType = $this->AccommodationTypes->get($id);
        if ($this->AccommodationTypes->delete($accommodationType)) {
            $this->Flash->success(__('The accommodation type has been deleted.'));
        } else {
            $this->Flash->error(__('The accommodation type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
