<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * IncomeSources Controller
 *
 * @property \App\Model\Table\IncomeSourcesTable $IncomeSources
 *
 * @method \App\Model\Entity\IncomeSource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IncomeSourcesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $incomeSources = $this->paginate($this->IncomeSources);

        $this->set(compact('incomeSources'));
    }

    /**
     * View method
     *
     * @param string|null $id Income Source id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incomeSource = $this->IncomeSources->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('incomeSource', $incomeSource);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $incomeSource = $this->IncomeSources->newEntity();
        if ($this->request->is('post')) {
            $incomeSource = $this->IncomeSources->patchEntity($incomeSource, $this->request->getData());
            if ($this->IncomeSources->save($incomeSource)) {
                $this->Flash->success(__('The income source has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The income source could not be saved. Please, try again.'));
        }
        $patients = $this->IncomeSources->Patients->find('list', ['limit' => 200]);
        $this->set(compact('incomeSource', 'patients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Income Source id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incomeSource = $this->IncomeSources->get($id, [
            'contain' => ['Patients']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incomeSource = $this->IncomeSources->patchEntity($incomeSource, $this->request->getData());
            if ($this->IncomeSources->save($incomeSource)) {
                $this->Flash->success(__('The income source has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The income source could not be saved. Please, try again.'));
        }
        $patients = $this->IncomeSources->Patients->find('list', ['limit' => 200]);
        $this->set(compact('incomeSource', 'patients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Income Source id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incomeSource = $this->IncomeSources->get($id);
        if ($this->IncomeSources->delete($incomeSource)) {
            $this->Flash->success(__('The income source has been deleted.'));
        } else {
            $this->Flash->error(__('The income source could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
