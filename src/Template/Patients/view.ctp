<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Patient $patient
 */
$uid= $this->Session->read('Auth.User.id')
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h3><?= 'Patient: '?>
            <?= strtoupper($patient->last_name ) ?>
            <?php if($patient->deleted==1){ ?>
                <?=  $this->Form->postLink(
                    '<bottom  class="btn btn-danger btn-icon-split" style="float: right;" >
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                <span class="text">Restore Patient Record</span>
            </bottom>',
                    ['action' => 'recover', $patient->id],
                    ['escape'=>false,'confirm' => __('Are you sure you want to restore this patient ?')]
                ) ?>
            <?php }?>
            <?php if($patient->deleted==0){ ?>
            <?=  $this->Form->postLink(
                '<bottom  class="btn btn-danger btn-icon-split" style="float: right;" >
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                <span class="text">Move to Recycle Bin</span>
            </bottom>',
                ['action' => 'delete', $patient->id],
                ['escape'=>false,'confirm' => __('Are you sure you want to move this patient to the recycle bin ?')]
            ) ?>
            <?php }?>
            <?= $this->Html->link('<bottom  class="btn btn-primary btn-icon-split" style="float: right; margin-right:10px;" >
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                <span class="text">Edit</span>
            </bottom>',
                ['action' => 'edit', $patient->id],
                ['escape'=>false]) ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <tr>
                    <th scope="row"><?= __('Date Captured') ?></th>
                    <td><?= h($patient->date_captured->format('d/m/Y')) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Contributor - Account ID') ?></th>
                    <td><?= $patient->has('staff') ? $this->Html->link($patient->staff->id, ['controller' => 'Staff', 'action' => 'view', $patient->staff->id]) : '' ?></td>

                </tr>
                <?php if ($is_admin || $is_staff){ ?>
                    <tr>
                        <th scope="row"><?= __('Last Name') ?></th>
                        <td><?= strtoupper($patient->last_name) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Middle Name') ?></th>
                        <td><?= h($patient->middle_name) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('First Name') ?></th>
                        <td><?= h($patient->first_name) ?></td>
                    </tr>
                <?php } ?>
                <?php if ($is_clinic && $patient->staff->id==$uid ){ ?>
                    <tr>
                        <th scope="row"><?= __('First Name') ?></th>
                        <td><?= h($patient->first_name) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Middle Name') ?></th>
                        <td><?= h($patient->middle_name) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Last Name') ?></th>
                        <td><?= h($patient->last_name) ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <th scope="row"><?= __('Gender') ?></th>
                    <td><?= $patient->has('gender') ? $patient->gender->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Marital Status') ?></th>
                    <td><?= $patient->has('marital_status') ? $patient->marital_status->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Date of Birth') ?></th>
                    <td><?= h($patient->dob->format('d/m/Y')) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Country Of Birth') ?></th>
                    <td><?= h($patient->country_of_birth) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Preferred Language') ?></th>
                    <td><?= h($patient->preferred_language) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Post Code') ?></th>
                    <td><?= h($patient->post_code) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Living Arrangement') ?></th>
                    <td><?= $patient->has('living_arrangement') ? $patient->living_arrangement->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Accommodation Type') ?></th>
                    <td><?= $patient->has('accommodation_type') ? $patient->accommodation_type->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Employment Type') ?></th>
                    <td><?= $patient->has('employment_type') ? $patient->employment_type->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Annual Income') ?></th>
                    <td><?= h($patient->annual_income) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Indigenous Or Islander') ?></th>
                    <td><?= h($patient->is_indigenous_or_islander) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Huntington Status') ?></th>
                    <td><?= $patient->has('huntington_status') ? $patient->huntington_status->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Line Of Inheritance') ?></th>
                    <td><?= $patient->has('huntington_inheritance') ? $patient->huntington_inheritance->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('If Research Participant') ?></th>
                    <td><?= $patient->if_research_participant ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Deceased Year') ?></th>
                    <td><?= h($patient->deceased_year) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Cause Of Death') ?></th>
                    <td><?= $patient->has('death_cause') ? $patient->death_cause->description : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Other Conditions') ?></th>
                    <td><?php foreach ($patient->other_conditions as $otherCondition): ?>
                            <?= ($otherCondition->description); echo '<br>' ?>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Income Sources') ?></th>
                    <td><?php foreach ($patient->income_sources as $incomeSources): ?>
                            <?= ($incomeSources->source); echo '<br>' ?>
                        <?php endforeach; ?>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
