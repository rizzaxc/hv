<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Patient $patient
 */
use Cake\I18n\Time;
use Cake\Collection\Collection;

$annual_income = ['<$15,000' => '<$15,000', '$15,000 - $20,000' => '$15,000 - $20,000',
    '$20,000 - $25,000' => '$20,000 - $25,000', '$25,000 - $30,000' => '$25,000 - $30,000',
    '$30,000 - $35,000' => '$30,000 - $35,000', '$35,000 - $40,000' => '$35,000 - $40,000',
    '$40,000 - $45,000' => '$40,000 - $45,000', '$45,000 - $50,000' => '$45,000 - $50,000',
    '$50,000 - $55,000' => '$50,000 - $55,000', '$55,000 - $60,000' => '$55,000 - $60,000',
    '$60,000 - $65,000' => '$60,000 - $65,000', '$65,000 - $70,000' => '$65,000 - $70,000',
    '$70,000 - $75,000' => '$70,000 - $75,000', '$75,000 - $80,000' => '$75,000 - $80,000',
    '$80,000 - $85,000' => '$80,000 - $85,000', '$85,000 - $90,000' => '$85,000 - $90,000',
    '$90,000 - $95,000' => '$90,000 - $95,000', '$95,000 - $100,000' => '$95,000 - $100,000',
    '$100,000 - $105,000' => '$100,000 - $105,000', '$105,000 - $110,000' => '$105,000 - $110,000',
    '$110,000 - $115,000' => '$110,000 - $115,000', '$115,000 - $120,000' => '$115,000 - $120,000',
    '$120,000 - $125,000' => '$120,000 - $125,000', '$125,000 - $130,000' => '$125,000 - $130,000',
    '$130,000 - $135,000' => '$130,000 - $135,000', '$135,000 - $140,000' => '$135,000 - $140,000',
    '$140,000 - $145,000' => '$140,000 - $145,000', '$145,000 - $150,000' => '$145,000 - $150,000',
    '$150,000 - $155,000' => '$150,000 - $155,000', '$155,000 - $160,000' => '$155,000 - $160,000',
    '$160,000 - $165,000' => '$160,000 - $165,000', '$165,000 - $170,000' => '$165,000 - $170,000',
    '$175,000 - $180,000' => '$175,000 - $180,000', '$180,000+' => '$180,000+'];


$indigenous_islander = ['Indigenous' => 'Indigenous', 'Torres Strait Islander' => 'Torres Strait Islander',
    'Both' => 'Both', 'No' => 'No'];

$yes_no = ['Yes' => 'Yes', 'No' => 'No'];

$line_of_inheritance = ['Paternal' => 'Paternal', 'Maternal' => 'Maternal',
    'Both' => 'Both', 'Unknown' => 'Unknown'];

$now = Time::now()->i18nFormat('yyyy-MM-dd');
Cake\I18n\Date::setToStringFormat('yyyy-MM-dd');
Cake\I18n\FrozenDate::setToStringFormat('yyyy-MM-dd');
// Country Picker

echo $this->Html->script('https://code.jquery.com/jquery-1.12.4.min.js', ['block' => 'countryPicker']);

echo $this->Html->script('countrypicker.js', ['block' => 'countryPicker']);
echo $this->Html->script('datepicker.js');
echo $this->fetch('countryPicker');
$uid= $this->Session->read('Auth.User.id')
//date picker

?>

<div class="card shadow mb-4">
    <?php $myTemplates=[
        'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>',
        //   'input' => '<input type="{{type}}" class="form-control is-invalid"  name="{{name}}"{{attrs}}/>',
        'inputContainerError' => '<div class="input {{type}}{{required}}">{{content}}{{error}}</div>',
        'error' => '<div class="invalid-feedback">{{content}}</div>'
    ];
    $this->Form->setTemplates($myTemplates) ?>
    <div class="card-header py-3">
        <h3><?= __('Edit Patient : ') ?>
            <?= strtoupper($patient->last_name) ?>
            <?= (',') ?>
            <?= strtoupper($patient->first_name) ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="patients form large-9 medium-8 columns container">
            <?= $this->Form->create($patient) ?>

            <div class="list-group list-group-horizontal" id="myList" role="tablist">
                <a class="list-group-item list-group-item-action active" data-toggle="list" href="#Basic_Information" role="tab">Basic Information</a>
                <a class="list-group-item list-group-item-action" data-toggle="list" href="#Geographical_Living" role="tab">Geographical & Living</a>
                <a class="list-group-item list-group-item-action" data-toggle="list" href="#Employment_Income" role="tab">Employment & Income</a>
                <a class="list-group-item list-group-item-action" data-toggle="list" href="#Huntington_Diagnosis" role="tab">Huntington's Diagnosis</a>
            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="Basic_Information" role="tabpanel">


                    <div class="form-row">
                        <div class="col-auto">
                            <!--                        echo $this->Form->control('date_captured', ['type'=>'text','class'=>'form-control ','value' => $now,'label'=>'Date Captured *','readonly'=>"readonly"]);-->
                            <?= $this->Form->control('date_captured', ['type'=>'text','class'=>'form-control ','value' => $now,'label'=>'Date Captured','readonly'=>"readonly"]); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto" >
                            <?= $this->Form->control('staff_id', ['options' => $staff,'value'=>$uid, 'disabled' => 'disabled','class'=>($this->Form->isFieldError('staff')) ? 'form-control is-invalid' : 'form-control','label'=>'Staff']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('first_name',['required'=>false,'class'=>($this->Form->isFieldError('first_name')) ? 'form-control is-invalid' : 'form-control','label'=>'First Name *']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('middle_name',['required'=>false,'class'=>'form-control','label'=>'Middle Name']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('last_name',['required'=>false,'class'=>($this->Form->isFieldError('last_name')) ? 'form-control is-invalid' : 'form-control','label'=>'Last Name *']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('gender_id', ['required'=>false,'empty'=>true,'options' => $genders, 'class' => ($this->Form->isFieldError('gender_id')) ? 'form-control is-invalid' : 'form-control','label'=>'Gender *']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto" >
                            <?= $this->Form->control('marital_status_id', ['required'=>false,'empty'=>true,'options' => $maritalStatuses, 'label'=>'Marital Status *', 'class' => ($this->Form->isFieldError('marital_status_id')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('dob', ['empty'=>true, 'required'=>false,'type'=>'text','id'=>'dob','class'=>($this->Form->isFieldError('dob')) ? 'form-control is-invalid' : 'form-control','label'=>'Date of Birth *']); ?>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="Geographical_Living" role="tabpanel">


                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('country_of_birth', ['required'=>false,'label' => 'Country Of Birth *',
                                'class'=>($this->Form->isFieldError('country_of_birth')) ? 'form-control countrypicker is-invalid' : 'form-control countrypicker', 'type' => 'select',
                                'data-default' => 'Australia']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?='Is the patient Indigenous or Torres Strait Islander? *' ?>
                            <?=  $this->Form->select('is_indigenous_or_islander',$indigenous_islander, ['required'=>false,'class'=>($this->Form->isFieldError('is_indigenous_or_islander')) ? 'form-control is-invalid' : 'form-control','empty' => true,]); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?=  $this->Form->control('preferred_language',['required'=>false,'label'=>'Preferred_language *','class'=>($this->Form->isFieldError('preferred_language')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?=  $this->Form->control('post_code',['required'=>false,'class'=>($this->Form->isFieldError('post_code')) ? 'form-control is-invalid' : 'form-control','label'=>'Post Code *']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?=  $this->Form->control('living_arrangement_id', ['required'=>false,'options' => $livingArrangements, 'label' =>  'Living Arrangement *', 'empty' => true,'required'=>false,'class'=>($this->Form->isFieldError('living_arrangement_id')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">

                            <?=  $this->Form->control('accommodation_type_id', ['required'=>false,'options' => $accommodationTypes, 'label' =>'Accommodation Type *', 'empty' => true, 'class' =>($this->Form->isFieldError('accommodation_type_id')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>

                </div>
                <div class="tab-pane" id="Employment_Income" role="tabpanel">

                    <div class="form-row">
                        <div class="col-auto">
                            <?= 'Employment Status * '; ?>
                            <?=  $this->Form->control('employment_type_id', ['required'=>false,'options' => $employmentTypes, 'label' => false, 'empty' => true, 'class' => ($this->Form->isFieldError('employment_type_id')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= 'Annual Income * '; ?>
                            <?= $this->Form->select('annual_income', $annual_income, ['empty' => true,'required'=>false,'class'=>($this->Form->isFieldError('annual_income')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('income_sources._ids', ['options' => $incomeSources,
                                'multiple' => 'checkbox','empty' => true, 'label' => 'Income Sources (Only select what applies) *','required'=>false,'class'=>($this->Form->isFieldError('income_sources._ids')) ? 'form-control-input is-invalid' : 'form-control-input']); ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="Huntington_Diagnosis" role="tabpanel">
                    <div class="form-row">
                        <div class="col-auto">
                            <?= 'Huntington Status * '; ?>
                            <?= $this->Form->control('huntington_status_id', ['required'=>false,'options' => $huntingtonStatuses, 'empty' => true, 'label' => false, 'class' => ($this->Form->isFieldError('huntington_status_id')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= 'What is the patient\'s line of Huntington\'s Disease inheritance? *'; ?>
                            <?= $this->Form->control('huntington_inheritance_id', ['required'=>false,'options' => $huntingtonInheritances, 'empty' => true, 'label' => false, 'class' =>($this->Form->isFieldError('huntington_inheritance_id')) ? 'form-control is-invalid' : 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= 'Given that the data will be de-identified, is the patient willing to have their data used in research by an external body? *' ?>
                            <br>
                            <?= $this->Form->radio('if_research_participant',$yes_no,['class'=>($this->Form->isFieldError('if_research_participant')) ? 'form-control-input is-invalid' : 'form-control-input']) ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">
                            <?= $this->Form->control('other_conditions._ids', ['required'=>false,'class'=>'form-control-input','options' => $otherConditions,
                                'multiple' => 'checkbox','empty' => true, 'label' => 'Any other conditions (Only select what applies)']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">

                            <?= $this->Form->control('deceased_year', ['label' => 'Deceased Year (If Applicable) ', 'class' => 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-auto">

                            <?=  $this->Form->control('death_cause_id', ['options' => $deathCauses, 'label' => 'Cause of Death (If Applicable) ', 'empty' => true, 'class' => 'form-control']); ?>
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn btn-primary btn-icon-split" style="margin-left: 45%;">
                <span class="text">Save</span>
            </button>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    $( function() {
        $( "#dob" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            minDate: "-60Y",
            yearRange: "-100:+0"
        })
    } );
</script>
