<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Patient[]|\Cake\Collection\CollectionInterface $patients
 */

?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h3><?= __('Patients') ?></h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>

                    <?php if ($is_admin ||$is_staff){ ?>
                        <th scope="col" style="color: #009dfe;"><?= __('Last Name') ?></th>
                        <th scope="col" style="color: #009dfe;"><?= __('First Name') ?></th>
                    <?php  } ?>
                    <th scope="col" style="color: #009dfe;"><?= __('Gender') ?></th>
                    <th scope="col" style="color: #009dfe;"><?= __('Date of Birth') ?></th>
                    <th scope="col" style="color: #009dfe;"><?= __('Country Of Birth') ?></th>
                    <th scope="col" style="color: #009dfe;"><?= __('Huntington Status') ?></th>
                    <th scope="col" style="color: #009dfe;"><?= __('Date Captured') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($patients as $patient): ?>
                    <tr>
                        <?php if ($is_admin || $is_staff){ ?>
                            <td><?= strtoupper($patient->last_name) ?></td>
                            <td><?= h($patient->first_name) ?></td>
                        <?php  } ?>
                        <td><?= $patient->has('gender') ? $patient->gender->description : '' ?></td>
                        <td><?= h($patient->dob->format('d/m/Y')) ?></td>
                        <td><?= h($patient->country_of_birth) ?></td>
                        <td><?= $patient->has('huntington_status') ? $patient->huntington_status->description : '' ?></td>
                        <td><?= h($patient->date_captured->format('d/m/Y')) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $patient->id]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->Html->script('jquery.dataTables.min.js', ['block' => 'demo']) ?>
<?= $this->Html->script('dataTables.bootstrap4.min.js', ['block' => 'demo']) ?>
<?= $this->Html->script('datatable.js', ['block' => 'demo']) ?>

