<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff $staff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $staff->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $staff->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Staff'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Managing Entities'), ['controller' => 'ManagingEntities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Managing Entity'), ['controller' => 'ManagingEntities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Patients'), ['controller' => 'Patients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Patient'), ['controller' => 'Patients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="staff form large-9 medium-8 columns content">
    <?= $this->Form->create($staff) ?>
    <fieldset>
        <legend><?= __('Edit Staff') ?></legend>
        <?php
            echo $this->Form->control('managing_entity_id', ['options' => $managingEntities, 'empty' => true]);
            echo $this->Form->control('role');
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('middle_name');
            echo $this->Form->control('password');
            echo $this->Form->control('username');
            echo $this->Form->control('phone_number');
            echo $this->Form->control('password_timestamp');
            echo $this->Form->control('token');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
