<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HuntingtonInheritance $huntingtonInheritance
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Huntington Inheritances'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Patients'), ['controller' => 'Patients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Patient'), ['controller' => 'Patients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="huntingtonInheritances form large-9 medium-8 columns content">
    <?= $this->Form->create($huntingtonInheritance) ?>
    <fieldset>
        <legend><?= __('Add Huntington Inheritance') ?></legend>
        <?php
            echo $this->Form->control('description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
