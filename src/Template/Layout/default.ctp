<?php
require('pageLink.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Huntington's Victoria</title>


    <!-- Custom fonts for this template-->

    <?= $this->Html->css('all.min.css', ['block' => 'default_css', 'type' => 'text/css']) ?>

    <?= $this->Html->css('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i', ['block' => 'default_css']) ?>


    <!-- Custom styles for this template-->
    <?= $this->Html->css('master/master.min.css', ['block' => 'default_css']) ?>
    <?= $this->Html->css('dataTables/dataTables.bootstrap4.min.css',['block'=>'default_css']) ?>
    <?= $this->fetch('default_css') ?>





</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->

        <a class="sidebar-brand d-flex align-items-center justify-content-center"  >
            <div class="sidebar-brand-icon " >
                <div class="title-img" >
                    <?= $this->Html->image('hvLogo.png') ?>
                </div>
            </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">
        <?php if ($is_admin ||$is_staff){ ?>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href = <?= $dashboard ?> >
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <?php } ?>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            REGISTRY DATA
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href = <?= $submissionForm?> >
                <i class="fas fa-fw fa-plus"></i>
                <span>Patient Submission Form</span>
            </a>
        </li>
        <?php if ($is_clinic){ ?>
            <li class="nav-item">
                <a class="nav-link" href = <?= $clinicPatient?> >
                    <i class="fas fa-fw fa-table"></i>
                    <span>Patient Data</span>
                </a>
            </li>
        <?php } ?>

        <?php if ($is_staff){ ?>
            <li class="nav-item">
                <a class="nav-link" href = <?= $patients?> >
                    <i class="fas fa-fw fa-table"></i>
                    <span>Patient Data</span>
                </a>
            </li>
        <?php } ?>

        <!-- Nav Item - Patients Data View -->
        <?php if ($is_admin ){ ?>
            <li class="nav-item">
                <a class="nav-link" href = <?= $patients?> >
                    <i class="fas fa-fw fa-table"></i>
                    <span>Patient Data</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href = <?= $recycle?> >
                    <i class="fas fa-fw fa-trash"></i>
                    <span>Recycle Bin</span>
                </a>
            </li>

            <!-- Heading -->
            <div class="sidebar-heading">
                FORM ADJUSTMENTS
            </div>


            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseConditions" aria-expanded="true" aria-controls="collapseConditions">
                    <i class="fas fa-fw fa-stethoscope"></i>
                    <span>Adjust Form Data</span>
                </a>
                <div id="collapseConditions" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item" href = <?= $accommodations ?> >Accommodation Types</a>
                        <a class="collapse-item" href = <?= $deathCauses ?> >Death Causes</a>
                        <a class="collapse-item" href = <?= $employmentTypes ?> >Employment Types</a>
                        <a class="collapse-item" href = <?= $genders ?> >Genders</a>
                        <a class="collapse-item" href = <?= $inheritances ?> >Huntington Inheritances</a>
                        <a class="collapse-item" href = <?= $statuses ?> >Huntington Statuses</a>
                        <a class="collapse-item" href = <?= $incomeSources ?> >Income Sources</a>
                        <a class="collapse-item" href = <?= $livingArrangements ?> >Living Arrangements</a>
                        <a class="collapse-item" href = <?= $marital ?> >Marital Statuses</a>
                        <a class="collapse-item" href = <?= $conditions ?> >Other Health Conditions</a>
                    </div>
                </div>

            </li>


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                ACCOUNT MANAGEMENT
            </div>

            <li class="nav-item">
                <a class="nav-link" href = <?= $staff?> >
                    <i class="fas fa-fw fa-user"></i>
                    <span>User Accounts</span>
                </a>
            </li>
            <!--<li class="nav-item">
                <a class="nav-link" href = <?= $verified?> >
                    <i class="fas fa-fw fa-id-card"></i>
                    <span>User Verification</span>
                </a>
            </li>-->
            <li class="nav-item">
                <a class="nav-link" href = <?= $entities?> >
                    <i class="fas fa-fw fa-door-open"></i>
                    <span>Contributing Clinics</span>
                </a>
            </li>
        <?php } ?>
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>



                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">


                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">

                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $this->request->session()->read('Auth.User.first_name')," ",$this->request->session()->read('Auth.User.middle_name')," ", $this->request->session()->read('Auth.User.last_name')?></span>
                            <?php   if ($is_admin) {?>
                                <i class="fas fa-user-lock fa-2x"></i>
                            <?php } ?>
                            <?php   if ($is_staff) {?>
                                <i class="fas fa-user-tie fa-2x"></i>
                            <?php } ?>
                            <?php   if ($is_clinic) {?>
                                <i class="fas fa-user-md fa-2x"></i>
                            <?php } ?>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                            <a class="dropdown-item" href="#\" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                            <a class="dropdown-item" href=<?= $resetPassword?> >
                                Reset Password
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <div class="container-fluid">
                    <?= $this->Flash->render() ?>
                </div>

                <?php echo $this->fetch("content") ?>
            </div>

        </div>
        <!-- /.container-fluid -->


        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    E-mail: info@huntingtonsvic.org.au |
                    Phone Number: 03 9818 6333 <br><br>
                    <span>Copyright &copy; Huntington's Victoria</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->
    </div>
</div>


<!-- End of Content Wrapper -->

<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href=<?= $logout?> >Logout</a>
            </div>
        </div>
    </div>
</div>



<!-- Bootstrap core JavaScript-->
<?= $this->Html->script('bootstrap/jquery.min.js', ['block' => 'default']) ?>
<?= $this->Html->script('bootstrap/bootstrap.bundle.min.js', ['block' => 'default']) ?>

<!-- Core plugin JavaScript-->
<?= $this->Html->script('core-plugin/jquery.easing.min.js', ['block' => 'default']); ?>

<!-- Custom scripts for all pages-->
<?= $this->Html->script('global/sb-admin-2.min.js', ['block' => 'default']) ?>
<?= $this->Html->script('global/jquery-ui.min.js', ['block' => 'default']) ?>

<?= $this->fetch('default'); ?>


</body>

</html>
