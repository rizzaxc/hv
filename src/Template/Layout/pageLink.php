<?php
// URL to different views
$dashboard = $this->Url->build([

    'controller' => 'dashboard',
    'action' => 'index'
]);

$submissionForm = $this->Url->build([

    'controller' => 'patients',
    'action' => 'add'
]);

$patients = $this->Url->build([

    'controller' => 'Patients',
    'action' => 'index'
]);

$staff = $this->Url->build([

    'controller' => 'Staff',
    'action' => 'index'
]);

#FIXME: What's this???
$verified = $this->Url->build([

    'controller' => 'Staff',
    'action' => 'verifiedemail'
]);

$conditions = $this->Url->build([

    'controller' => 'OtherConditions',
    'action' => 'index'
]);


$entities = $this->Url->build([

    'controller' => 'ManagingEntities',
    'action' => 'index'
]);

$incomeSources = $this->Url->build([

    'controller' => 'IncomeSources',
    'action' => 'index'
]);

$symptoms = $this->Url->build([

    'controller' => 'HuntingtonSymptoms',
    'action' => 'index'
]);
$genders = $this->Url->build([

    'controller' => 'Genders',
    'action' => 'index'
]);


$accommodations= $this->Url->build([

    'controller' => 'AccommodationTypes',
    'action' => 'index'
]);

$deathCauses= $this->Url->build([

    'controller' => 'DeathCauses',
    'action' => 'index'
]);

$employmentTypes=$this->Url->build([

    'controller' => 'EmploymentTypes',
    'action' => 'index'
]);

$inheritances=$this->Url->build([

    'controller' => 'HuntingtonInheritances',
    'action' => 'index'
]);

$statuses=$this->Url->build([

    'controller' => 'HuntingtonStatuses',
    'action' => 'index'
]);

$livingArrangements=$this->Url->build([

    'controller' => 'LivingArrangements',
    'action' => 'index'
]);

$marital=$this->Url->build([

    'controller' => 'MaritalStatuses',
    'action' => 'index'
]);


$logout = $this->Url->build([

    'controller' => 'Staff',
    'action' => 'logout'
]);

$clinicPatient = $this->Url->build([

    'controller' => 'Patients',
    'action' => 'ownpatient'
]);

$resetPassword = $this->Url->build([

    'controller' => 'Staff',
    'action' => 'resetPassword'
]);

$recycle = $this->Url->build([

    'controller' => 'Patients',
    'action' => 'recycle'
]);
?>
