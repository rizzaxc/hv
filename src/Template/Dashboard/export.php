<?php
use Cake\Datasource\ConnectionManager;

function patientToCSV() {
    // Output header so the file is downloaded
    header('Content-type: csv');
    header('Content-disposition: attachment; filename = patients.csv');

    $connection = ConnectionManager::get('default');
    $query = $connection
        ->execute(
            'SELECT
                        patients.id,
                        patients.dob,
                        patients.country_of_birth,
                        patients.if_research_participant,
                        patients.deceased_year,
                        patients.annual_income,
                        patients.post_code,
                        patients.preferred_language,
                        patients.is_indigenous_or_islander,
                        g.description as gender,
                        ms.description as marital_status,
                        hs.description as huntington_status,
                        a.description as accommodation,
                        dc.description as death_cause,
                        et.description as employment,
                        hi.description as huntington_inheritance,
                        la.description as living_arrangement
                    FROM patients
                    JOIN genders g on patients.gender_id = g.id
                    JOIN marital_statuses ms on patients.marital_status_id = ms.id
                    JOIN huntington_statuses hs on patients.huntington_status_id = hs.id
                    JOIN accommodation_types a on patients.accommodation_type_id = a.id
                    JOIN death_causes dc on patients.death_cause_id = dc.id
                    JOIN employment_types et on patients.employment_type_id = et.id
                    JOIN huntington_inheritances hi on patients.huntington_inheritance_id = hi.id
                    JOIN living_arrangements la on patients.living_arrangement_id = la.id
                    ORDER BY patients.dob
                    '
        )
        ->fetchAll('assoc');



    $fp = fopen('php://output', 'w');

    // Add column names to file
    $column = array(
        'Patient ID',
        'Day of birth',
        'Country of birth',
        'Research participant',
        'Deceased year',
        'Annual_income',
        'Post_code',
        'Preferred language',
        'Indigenous or Islander',
        'Gender',
        'Marital Status',
        'Huntington Status',
        'Accommodation',
        'Death cause',
        'Employment',
        'Huntington inheritance',
        'Living arrangement'
    );
    fputcsv($fp, $column);

    // Export data to file
    foreach ($query as $patient) {
        $info = array(
            $patient['id'],
            $patient['dob'],
            $patient['country_of_birth'],
            $patient['if_research_participant'],
            $patient['deceased_year'],
            $patient['annual_income'],
            $patient['post_code'],
            $patient['preferred_language'],
            $patient['is_indigenous_or_islander'],
            $patient['gender'],
            $patient['marital_status'],
            $patient['huntington_status'],
            $patient['accommodation'],
            $patient['death_cause'],
            $patient['employment'],
            $patient['huntington_inheritance'],
            $patient['living_arrangement'],
        );
        fputcsv($fp, $info);
    }

    fclose($fp);
    exit();

}

function otherConditionToCSV() {
    header('Content-type: csv');
    header('Content-disposition: attachment; filename = conditions.csv');

    $connection = ConnectionManager::get('default');
    $query = $connection
        ->execute(
        'SELECT
                    oc.description as description,
                    patient_id as id
                FROM other_conditions_patients
                JOIN other_conditions oc on other_conditions_patients.other_condition_id = oc.id
                ORDER BY oc.description
                '
        )
        ->fetchAll('assoc');

    $fp = fopen('php://output', 'w');
    $column = array('Description', 'Patient ID');

    fputcsv($fp, $column);

    // Export data to file
    foreach ($query as $data) {
        $line = array($data['description'], $data['id']);
        fputcsv($fp, $line);
    }

    fclose($fp);
    exit();
}

if (array_key_exists('patientToCSV',$_POST)) {
    patientToCSV();
}

if (array_key_exists('conditionToCSV', $_POST)) {
    otherConditionToCSV();
}
