<?php
use Cake\Datasource\ConnectionManager;


// Load the GoogleCharts library
echo "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>";

// Code for Patient Distribution
$connection = ConnectionManager::get('default');
$query = $connection
    ->newQuery()
    ->select(['post_code', 'gender_id', 'huntington_inheritance_id'])
    ->from('patients');
$patientCount = 0;

# states
$vic = 0;
$nsw = 0;
$tas = 0;
$nt = 0;
$sa = 0;
$wa = 0;
$qld = 0;
$act = 0;

# genders
$male = 0;
$female = 0;
$intersex = 0;
$genderFluid = 0;
$transMale = 0;
$transFemale = 0;
$nonBinary = 0;
$others = 0;

# line of inheritances
$maternal = 0;
$paternal = 0;
$both = 0;
$unknown = 0;



# process data
foreach ($query as $data) {
    $patientCount += 1;
    $postcode = $data['post_code'];
    if ($postcode >= '1000' and $postcode <= '2999') $nsw += 1;
    elseif ($postcode >= '3000' and $postcode <= '3999') $vic += 1;
    elseif ($postcode >= '4000' and $postcode <= '4999') $qld += 1;
    elseif ($postcode >= '5000' and $postcode <= '5799') $sa += 1;
    elseif ($postcode >= '6000' and $postcode <= '6797') $wa += 1;
    elseif ($postcode >= '7000' and $postcode <= '7799') $tas += 1;
    elseif ($postcode >= '0800' and $postcode <= '0899') $nt += 1;
    elseif ($postcode >= '0200' and $postcode <= '0299') $act += 1;

    $gender = $data['gender_id'];
    switch ($gender) {
        case 1:
            $male++;
            break;
        case 2:
            $female++;
            break;
        case 3:
            $intersex++;
            break;
        case 4:
            $genderFluid++;
            break;
        case 5:
            $transMale++;
            break;
        case 6:
            $transFemale++;
            break;
        case 7:
            $nonBinary++;
            break;
        default:
            $others++;
            break;
    }

    $inheritance = $data['huntington_inheritance_id'];
    switch ($inheritance) {
        case 1:
            $paternal++;
            break;
        case 2:
            $maternal++;
            break;
        case 3:
            $both++;
            break;
        default:
            $unknown++;
            break;
    }

}

echo $tas;


echo "<script type=\"text/javascript\">

    google.charts.load('current', {
        'packages':['geochart', 'corechart'],
        // Note: need to get a mapsApiKey for the project. using default key
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
    });
    google.charts.setOnLoadCallback(drawMap);
    
    google.charts.setOnLoadCallback(drawPatientByStatePieChart);
    
    google.charts.setOnLoadCallback(drawGenderPieChart);
    
    google.charts.setOnLoadCallback(drawInheritancePieChart);
    
    function drawInheritancePieChart() {
        var data = google.visualization.arrayToDataTable([
            ['Type', 'No of Patients'],
            ['Paternal', $paternal],
            ['Maternal', $maternal],
            ['Both', $both],
            ['Unknown', $unknown]
        ]);
        
        var options = {
            'chartArea': {width:500,height:315},
            'legend': {position: 'none', textStyle: {color: 'blue', fontSize: 20}},
            'width': 500,
            'height': 315,
            

        };

        var chart = new google.visualization.PieChart(document.getElementById('inheritance_chart_div'));

        chart.draw(data, options);
    }

    
     function drawGenderPieChart() {
        var data = google.visualization.arrayToDataTable([
            ['Gender', 'No of Patients'],
            ['Male', $male],
            ['Female', $female],
            ['Intersex', $intersex],
            ['Gender fluid', $genderFluid],
            ['Trans Male', $transMale],
            ['Trans Female', $transFemale],
            ['Non Binary', $nonBinary],
            ['Others', $others],
        ]);
        
        var options = {
            'chartArea': {width:500,height:315},
            'legend': {position: 'right', textStyle: {color: 'blue', fontSize: 20}},
            'width': 500,
            'height': 315,
            

        };

        var chart = new google.visualization.PieChart(document.getElementById('gender_chart_div'));

        chart.draw(data, options);
    }

    
    function drawPatientByStatePieChart() {
          var data = google.visualization.arrayToDataTable([
          ['State', 'No of Patients'],
          ['VIC', $vic],
          ['NSW', $nsw],
          ['QLD', $qld],
          ['SA', $sa],
          ['WA', $wa],
          ['NT', $nt],
          ['TAS', $tas],
          ['ACT', $act]

        ]);

        var options = {
            'chartArea': {width:500,height:315},
            'legend': {position: 'labeled', textStyle: {color: 'blue', fontSize: 20}},
            'width': 500,
            'height': 315,
            

        };

        var chart = new google.visualization.PieChart(document.getElementById('patient_by_state_div'));

        chart.draw(data, options);
    }

    function drawMap() {
        var mapData = google.visualization.arrayToDataTable([
            ['States', 'Patients'],
            ['Victoria', $vic],
            ['New South Wales', $nsw],
            ['Western Australia', $wa],
            ['Queensland', $qld],
            ['South Australia', $sa],
            ['Northern Territory', $nt],
            ['Tasmania', $tas],
            ['Australian Capital Territory', $act]

        ]);

        var options = {
            'region': 'AU',
            'resolution': 'provinces',
            'title': 'Map Of Huntington AU',
            'enableRegionInteractivity': 'true',
            'domain': 'AU',
            'magnifyingGlass': {enable: true, zoomFactor: 15.0},
        };

        var chart = new google.visualization.GeoChart(document.getElementById('map_div'));

        chart.draw(mapData, options);
    }
</script>";
