<?php
$this->assign('title', 'Dashboard');
require 'export.php';
require 'visualization.php';

?>


<!-- Begin Content Placeholder -->
<div class="container-fluid">
    <?php if ($is_admin ||$is_staff){ ?>
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>-->
        </div>

        <!-- Content Row -->
        <div class="row">
        <?php if ($is_admin ){ ?>
            <!-- Patient Export Card -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">

                        <div class="row no-gutters align-items-center">
                            <form method="post">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Export Patient Data</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        <button type="submit" value="Export Patient Data" name="patientToCSV">Download</button>

                                    </div>
                                </div>
                            </form>
                            <div class="col-auto">
                                <i class="fas fa-download fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Condition Export Card -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <form method="post">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Export Other Condition</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        <button type="submit" value="Export Other Condition Data" name="conditionToCSV">Download</button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-auto">
                                <i class="fas fa-download fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Pending Account Requests Card -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <a href="http://ie.infotech.monash.edu/team23/iteration3/staff" style="text-decoration: none">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Account Requests</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $pending?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-comments fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            </div>
        <?php  } ?>
        <!-- Content Row -->

        <div class="row">

            <!--         Area Chart -->
            <!--        <div class="col-xl-8 col-lg-7">-->
            <!--            <div class="card shadow mb-4">-->
            <!--                 Card Header - Dropdown -->
            <!--                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">-->
            <!--                    <h6 class="m-0 font-weight-bold text-primary">Patient Distribution</h6>-->
            <!--                    <div class="dropdown no-arrow">-->
            <!--                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
            <!--                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>-->
            <!--                        </a>-->
            <!--                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">-->
            <!--                            <div class="dropdown-header">Dropdown Header:</div>-->
            <!--                            <a class="dropdown-item" href="#">Action</a>-->
            <!--                            <a class="dropdown-item" href="#">Another action</a>-->
            <!--                            <div class="dropdown-divider"></div>-->
            <!--                            <a class="dropdown-item" href="#">Something else here</a>-->
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--                 Card Body -->
            <!--                <div class="card-body">-->
            <!---->
            <!--                </div>-->
            <!--            </div>-->
            <!--        </div>-->

            <!-- Au Map -->
            <div class="col-xl-6 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Patient Distribution Map</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div id="map_div"></div>
                        <div class="total_count_overlay" style="float: right; font-size: 16px; font-weight: bold">
                            <?= $this->Html->image('person.svg'), $patientCount ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-xl-6 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Patient Percentage Distribution by States</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div id="patient_by_state_div"></div>
                        <div class="total_count_overlay" style="float: right; font-size: 16px; font-weight: bold">
                            <?= $this->Html->image('person.svg'), $patientCount ?>
                        </div>
                    </div>
                </div>
            </div>




        </div>

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-6 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Gender Distribution</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div id="gender_chart_div"></div>
                        <div class="total_count_overlay" style="float: right; font-size: 16px; font-weight: bold">
                            <?= $this->Html->image('person.svg'), $patientCount ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Inheritance Distribution</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div id="inheritance_chart_div"></div>
                        <div class="total_count_overlay" style="float: right; font-size: 16px; font-weight: bold">
                            <?= $this->Html->image('person.svg'), $patientCount ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
</div>
<!-- /.container-fluid -->

<!-- End of Content Placeholder -->

