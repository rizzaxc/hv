<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HuntingtonStatus[]|\Cake\Collection\CollectionInterface $huntingtonStatuses
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Huntington Status'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Patients'), ['controller' => 'Patients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Patient'), ['controller' => 'Patients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="huntingtonStatuses index large-9 medium-8 columns content">
    <h3><?= __('Huntington Statuses') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($huntingtonStatuses as $huntingtonStatus): ?>
            <tr>
                <td><?= $this->Number->format($huntingtonStatus->id) ?></td>
                <td><?= h($huntingtonStatus->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $huntingtonStatus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $huntingtonStatus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $huntingtonStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $huntingtonStatus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
