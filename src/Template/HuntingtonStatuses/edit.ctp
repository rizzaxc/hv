<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HuntingtonStatus $huntingtonStatus
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $huntingtonStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $huntingtonStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Huntington Statuses'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Patients'), ['controller' => 'Patients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Patient'), ['controller' => 'Patients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="huntingtonStatuses form large-9 medium-8 columns content">
    <?= $this->Form->create($huntingtonStatus) ?>
    <fieldset>
        <legend><?= __('Edit Huntington Status') ?></legend>
        <?php
            echo $this->Form->control('description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
