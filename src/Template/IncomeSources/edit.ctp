<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\IncomeSource $incomeSource
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $incomeSource->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $incomeSource->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Income Sources'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Patients'), ['controller' => 'Patients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Patient'), ['controller' => 'Patients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incomeSources form large-9 medium-8 columns content">
    <?= $this->Form->create($incomeSource) ?>
    <fieldset>
        <legend><?= __('Edit Income Source') ?></legend>
        <?php
            echo $this->Form->control('description');
            echo $this->Form->control('patients._ids', ['options' => $patients]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
