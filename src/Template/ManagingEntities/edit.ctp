<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ManagingEntity $managingEntity
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $managingEntity->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $managingEntity->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Managing Entities'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Account Request Info'), ['controller' => 'AccountRequestInfo', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Account Request Info'), ['controller' => 'AccountRequestInfo', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Staff'), ['controller' => 'Staff', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Staff'), ['controller' => 'Staff', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="managingEntities form large-9 medium-8 columns content">
    <?= $this->Form->create($managingEntity) ?>
    <fieldset>
        <legend><?= __('Edit Managing Entity') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('post_code');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
