<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ManagingEntity[]|\Cake\Collection\CollectionInterface $managingEntities
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Managing Entity'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Account Request Info'), ['controller' => 'AccountRequestInfo', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Account Request Info'), ['controller' => 'AccountRequestInfo', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Staff'), ['controller' => 'Staff', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Staff'), ['controller' => 'Staff', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="managingEntities index large-9 medium-8 columns content">
    <h3><?= __('Managing Entities') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('post_code') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($managingEntities as $managingEntity): ?>
            <tr>
                <td><?= $this->Number->format($managingEntity->id) ?></td>
                <td><?= h($managingEntity->name) ?></td>
                <td><?= h($managingEntity->post_code) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $managingEntity->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $managingEntity->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $managingEntity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $managingEntity->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
