<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ManagingEntity $managingEntity
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Managing Entity'), ['action' => 'edit', $managingEntity->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Managing Entity'), ['action' => 'delete', $managingEntity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $managingEntity->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Managing Entities'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Managing Entity'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Account Request Info'), ['controller' => 'AccountRequestInfo', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account Request Info'), ['controller' => 'AccountRequestInfo', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Staff'), ['controller' => 'Staff', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Staff'), ['controller' => 'Staff', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="managingEntities view large-9 medium-8 columns content">
    <h3><?= h($managingEntity->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($managingEntity->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Post Code') ?></th>
            <td><?= h($managingEntity->post_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($managingEntity->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Account Request Info') ?></h4>
        <?php if (!empty($managingEntity->account_request_info)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Code') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Managing Entity Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($managingEntity->account_request_info as $accountRequestInfo): ?>
            <tr>
                <td><?= h($accountRequestInfo->id) ?></td>
                <td><?= h($accountRequestInfo->code) ?></td>
                <td><?= h($accountRequestInfo->timestamp) ?></td>
                <td><?= h($accountRequestInfo->email) ?></td>
                <td><?= h($accountRequestInfo->managing_entity_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'AccountRequestInfo', 'action' => 'view', $accountRequestInfo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'AccountRequestInfo', 'action' => 'edit', $accountRequestInfo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'AccountRequestInfo', 'action' => 'delete', $accountRequestInfo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountRequestInfo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Staff') ?></h4>
        <?php if (!empty($managingEntity->staff)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Managing Entity Id') ?></th>
                <th scope="col"><?= __('Role') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Middle Name') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col"><?= __('Phone Number') ?></th>
                <th scope="col"><?= __('Password Timestamp') ?></th>
                <th scope="col"><?= __('Token') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($managingEntity->staff as $staff): ?>
            <tr>
                <td><?= h($staff->id) ?></td>
                <td><?= h($staff->managing_entity_id) ?></td>
                <td><?= h($staff->role) ?></td>
                <td><?= h($staff->first_name) ?></td>
                <td><?= h($staff->last_name) ?></td>
                <td><?= h($staff->middle_name) ?></td>
                <td><?= h($staff->password) ?></td>
                <td><?= h($staff->username) ?></td>
                <td><?= h($staff->phone_number) ?></td>
                <td><?= h($staff->password_timestamp) ?></td>
                <td><?= h($staff->token) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Staff', 'action' => 'view', $staff->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Staff', 'action' => 'edit', $staff->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Staff', 'action' => 'delete', $staff->id], ['confirm' => __('Are you sure you want to delete # {0}?', $staff->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
