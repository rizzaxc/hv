<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccommodationType[]|\Cake\Collection\CollectionInterface $accommodationTypes
 */
$new = $this->Url->build([

    'controller' => 'MaritalStatuses',
    'action' => 'add'
]);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h3><?= __('Marital Statuses') ?>



            <a  class="btn btn-primary btn-icon-split" style="float:right" href=<?= $new ?>  >
                    <span class="icon text-white-50">
                      <i class="far fa-address-book"></i>
                    </span>
                <span class="text">Add New Marital Status</span>
            </a>
        </h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>

                    <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($maritalStatuses as $maritalStatus): ?>
                    <tr>

                        <td><?= h($maritalStatus->description) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $maritalStatus->id]) ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <!--<div class="paginator" style="text-align:center">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
        </div>-->
    </div>
</div>
<?= $this->Html->script('jquery.dataTables.min.js', ['block' => 'demo']) ?>
<?= $this->Html->script('dataTables.bootstrap4.min.js', ['block' => 'demo']) ?>
<?= $this->Html->script('datatable.js', ['block' => 'demo']) ?>
