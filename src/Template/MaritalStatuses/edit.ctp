<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccommodationType $accommodationType
 */
?>


<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h3><?= __('Edit Marital Status ') ?></h3>
    </div>
    <div class="card-body">

        <div class="staff form large-9 medium-8 columns container">
            <?= $this->Form->create($maritalStatus) ?>


            <div class="form-row ">
                <div class="col-md-4">
                    <?php echo $this->Form->control('status',['class'=>'form-control']); ?>
                </div>
            </div>

            <button class="btn btn-primary btn-icon-split" style="margin-left: 45%;">
                <span class="text">Save</span>
            </button>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
