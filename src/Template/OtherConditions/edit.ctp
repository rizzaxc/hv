<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OtherCondition $otherCondition
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $otherCondition->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $otherCondition->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Other Conditions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Patients'), ['controller' => 'Patients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Patient'), ['controller' => 'Patients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="otherConditions form large-9 medium-8 columns content">
    <?= $this->Form->create($otherCondition) ?>
    <fieldset>
        <legend><?= __('Edit Other Condition') ?></legend>
        <?php
            echo $this->Form->control('description');
            echo $this->Form->control('patients._ids', ['options' => $patients]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
