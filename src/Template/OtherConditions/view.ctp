<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OtherCondition $otherCondition
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Other Condition'), ['action' => 'edit', $otherCondition->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Other Condition'), ['action' => 'delete', $otherCondition->id], ['confirm' => __('Are you sure you want to delete # {0}?', $otherCondition->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Other Conditions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Other Condition'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Patients'), ['controller' => 'Patients', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Patient'), ['controller' => 'Patients', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="otherConditions view large-9 medium-8 columns content">
    <h3><?= h($otherCondition->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($otherCondition->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($otherCondition->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Patients') ?></h4>
        <?php if (!empty($otherCondition->patients)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Staff Id') ?></th>
                <th scope="col"><?= __('Gender Id') ?></th>
                <th scope="col"><?= __('Marital Status Id') ?></th>
                <th scope="col"><?= __('Huntington Status Id') ?></th>
                <th scope="col"><?= __('Accommodation Type Id') ?></th>
                <th scope="col"><?= __('Employment Type Id') ?></th>
                <th scope="col"><?= __('Huntington Inheritance Id') ?></th>
                <th scope="col"><?= __('Living Arrangement Id') ?></th>
                <th scope="col"><?= __('Death Cause Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Middle Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Dob') ?></th>
                <th scope="col"><?= __('Country Of Birth') ?></th>
                <th scope="col"><?= __('If Research Participant') ?></th>
                <th scope="col"><?= __('Deceased Year') ?></th>
                <th scope="col"><?= __('Annual Income') ?></th>
                <th scope="col"><?= __('Preferred Language') ?></th>
                <th scope="col"><?= __('Is Indigenous Or Islander') ?></th>
                <th scope="col"><?= __('Date Captured') ?></th>
                <th scope="col"><?= __('Deleted') ?></th>
                <th scope="col"><?= __('Post Code') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($otherCondition->patients as $patients): ?>
            <tr>
                <td><?= h($patients->id) ?></td>
                <td><?= h($patients->staff_id) ?></td>
                <td><?= h($patients->gender_id) ?></td>
                <td><?= h($patients->marital_status_id) ?></td>
                <td><?= h($patients->huntington_status_id) ?></td>
                <td><?= h($patients->accommodation_type_id) ?></td>
                <td><?= h($patients->employment_type_id) ?></td>
                <td><?= h($patients->huntington_inheritance_id) ?></td>
                <td><?= h($patients->living_arrangement_id) ?></td>
                <td><?= h($patients->death_cause_id) ?></td>
                <td><?= h($patients->first_name) ?></td>
                <td><?= h($patients->middle_name) ?></td>
                <td><?= h($patients->last_name) ?></td>
                <td><?= h($patients->dob) ?></td>
                <td><?= h($patients->country_of_birth) ?></td>
                <td><?= h($patients->if_research_participant) ?></td>
                <td><?= h($patients->deceased_year) ?></td>
                <td><?= h($patients->annual_income) ?></td>
                <td><?= h($patients->preferred_language) ?></td>
                <td><?= h($patients->is_indigenous_or_islander) ?></td>
                <td><?= h($patients->date_captured) ?></td>
                <td><?= h($patients->deleted) ?></td>
                <td><?= h($patients->post_code) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Patients', 'action' => 'view', $patients->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Patients', 'action' => 'edit', $patients->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Patients', 'action' => 'delete', $patients->id], ['confirm' => __('Are you sure you want to delete # {0}?', $patients->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
