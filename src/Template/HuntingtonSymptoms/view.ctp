<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HuntingtonSymptom $huntingtonSymptom
 */
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h3><?= h($huntingtonSymptom->id) ?>
            <?=  $this->Form->postLink(
                '<bottom  class="btn btn-danger btn-icon-split" style="float: right;" >
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                <span class="text">Delete</span>
            </bottom>',
                ['action' => 'delete', $huntingtonSymptom->id],
                ['escape'=>false,'confirm' => __('Are you sure you want to delete this Huntington symptom?', $huntingtonSymptom->id)]
            ) ?>
            <?= $this->Html->link('<bottom  class="btn btn-primary btn-icon-split" style="float: right; margin-right:10px;" >
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                <span class="text">Edit</span>
            </bottom>',
                ['action' => 'edit', $huntingtonSymptom->id],
                ['escape'=>false]) ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <tr>
                    <th scope="row"><?= __('Description') ?></th>
                    <td><?= h($huntingtonSymptom->description) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($huntingtonSymptom->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
