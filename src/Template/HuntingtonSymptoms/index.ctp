<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HuntingtonSymptom[]|\Cake\Collection\CollectionInterface $huntingtonSymptoms
 */
$new_symptoms = $this->Url->build([

    'controller' => 'HuntingtonSymptoms',
    'action' => 'add'
]);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h3><?= __('Huntington Symptoms') ?>
            <a  class="btn btn-primary btn-icon-split" style="float:right" href=<?= $new_symptoms ?>  >
                    <span class="icon text-white-50">
                      <i class="far fa-address-book"></i>
                    </span>
                <span class="text">Add a new Huntington Symptoms</span>
            </a>
        </h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($huntingtonSymptoms as $huntingtonSymptom): ?>
                    <tr>
                        <td><?= $this->Number->format($huntingtonSymptom->id) ?></td>
                        <td><?= h($huntingtonSymptom->description) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $huntingtonSymptom->id]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="paginator" style="text-align:center">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
        </div>
    </div>
</div>
