<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HuntingtonSymptom $huntingtonSymptom
 */
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h3><?= __('Edit Huntington Symptom') ?></h3>
    </div>
    <div class="card-body">
        <div class="huntingtonSymptoms form large-9 medium-8 columns container">
            <?= $this->Form->create($huntingtonSymptom) ?>
            <fieldset>
                <?php
                echo $this->Form->control('description');
                ?>
            </fieldset>
            <button class="btn btn-primary btn-icon-split" style="margin-left: 50%;">
                <span class="text">Submit</span>
            </button>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
